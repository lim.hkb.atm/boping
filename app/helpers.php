<?php

function getMonthlyPartitionRange($start_date, $end_date){
    // Check if Start date > End date 
    if(strtotime($end_date) < strtotime($start_date)) $end_date = $start_date;

    $start_year = date("Y", strtotime($start_date));
    $start_month = date("m", strtotime($start_date));

    $end_year = date("Y", strtotime($end_date));
    $end_month = date("m", strtotime($end_date));

    $range_partition = "p" . $start_year . $start_month;
    $start_month += 1;
    
    for($start_year; $start_year <= $end_year; $start_year++){
        // Set end month to last if still have next year
        if($start_year != $end_year) {
            $end_month = 12;
        }else {
            $end_month = date("m", strtotime($end_date));
        }

        for($start_month; $start_month <= $end_month; $start_month++) {
            $range_partition .= ",p" . $start_year . sprintf("%02d", $start_month);
            if($start_month == 12) {
                $start_month = 1;
                break;
            }
        }
    }
    return $range_partition;
}
  
function getQuartalPartition($date){
    $monthnumber=date('n',strtotime($date));
    $quarter=floor(($monthnumber - 1) / 3) + 1;
    $q="q".$quarter;
    $year=date('Y',strtotime($date));
    $partition="p".$year.$q;

    return $partition;
}
  
function getQuartal($date){
    $monthnumber=date('n',strtotime($date));
    $quarter=floor(($monthnumber - 1) / 3) + 1;
    return $quarter;
}