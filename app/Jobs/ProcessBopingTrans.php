<?php

namespace App\Jobs;

use App\Http\Traits\SumWinloseTrait;
use App\Models\BopingTrans;
use App\Models\Player;
use App\Models\SumWinlose;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class ProcessBopingTrans implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SumWinloseTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;
    
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // process list of bet details
        $is_bet_detail_exist = isset($this->data['Data']['BetDetails']);
        if($is_bet_detail_exist){
            $this->storeBopingTrans($this->data['Data']['BetDetails']);
        }

        // Process list of bet number details
        $is_bet_number_detail_exist = isset($this->data['Data']['BetNumberDetails']);
        if($is_bet_number_detail_exist){
            $this->storeBopingTrans($this->data['Data']['BetNumberDetails']);
        }   
    }
}
