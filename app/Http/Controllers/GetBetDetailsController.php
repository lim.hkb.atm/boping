<?php

namespace App\Http\Controllers;

use App\Http\Traits\SumWinloseTrait;
use App\Jobs\ProcessBopingTrans;
use App\Models\BopingTrans;
use App\Models\Player;
use App\Models\SportType;
use App\Models\SumWinlose;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\DB;

class GetBetDetailsController extends Controller
{
    public function index()
    {
        /* Query alternative for sum_winlose */
        // $boping_trans = BopingTrans::select('winlost_datetime', 'sport_type', 'vendor_member_id',
        // DB::raw('SUM(stake)'),
        // DB::raw('SUM(CASE WHEN winlost_amount > 0 THEN winlost_amount ELSE 0 END) AS win'),
        // DB::raw('ABS(SUM(CASE WHEN winlost_amount < 0 THEN winlost_amount ELSE 0 END)) AS lose')
        // )
        // ->groupBy(['winlost_datetime', 'sport_type', 'vendor_member_id'])->toSql();

        $vendor_id = '0dfz5qetxt';
        $version_key = 0;

        $latest_boping_trans = BopingTrans::select('version_key')->orderBy('version_key', 'desc')->first();
        if($latest_boping_trans) {
            $version_key = $latest_boping_trans->version_key;
        }

        while(true){
            $response = Http::asForm()->post('http://x9f2tsa.bw6688.com/api/GetBetDetail/', [
                'vendor_id' => $vendor_id,
                'version_key' => $version_key
            ]);
    
            $data = json_decode($response->body(), true);

            if($version_key == $data['Data']['last_version_key']){
                break;
            }

            ProcessBopingTrans::dispatch($data);
            
            $version_key = $data['Data']['last_version_key'];
        }

        return 'Process BOPING trans Successful';
    }

    public function betHistoryReport (Request $request) {
        $transaction_start_datetime = $request->query('transaction_start_datetime');
        $transaction_end_datetime = $request->query('transaction_end_datetime');
        $match_start_datetime = $request->query('match_start_datetime');
        $match_end_datetime = $request->query('match_end_datetime');
        $settlement_start_datetime = $request->query('settlement_start_datetime');
        $settlement_end_datetime = $request->query('settlement_end_datetime');
        $sport_type = $request->query('sport_type');
        $player = $request->query('player');

        $list_boping_trans = BopingTrans::query();

        if ($transaction_start_datetime) {
            $list_boping_trans->where('transaction_time', '>=', $transaction_start_datetime);
        }
        
        if ($transaction_end_datetime) {
            $list_boping_trans->where('transaction_time', '<=', $transaction_end_datetime);
        }

        if ($match_start_datetime) {
            $list_boping_trans->where('match_datetime', '>=', $match_start_datetime);
        }
        
        if ($match_end_datetime) {
            $list_boping_trans->where('match_datetime', '<=', $match_end_datetime);
        }

        if ($settlement_start_datetime) {
            $list_boping_trans->where('settlement_time', '>=', $settlement_start_datetime);
        }
        
        if ($settlement_end_datetime) {
            $list_boping_trans->where('settlement_time', '<=', $settlement_end_datetime);
        }

        if ($sport_type) {
            $list_boping_trans->where('sport_type', $sport_type);
        }

        if ($player) {
            $list_boping_trans->where('vendor_member_id', $player);
        }

        $list_boping_trans->with(['player', 'sport', 'bet']);
        
        $list_boping_trans = $list_boping_trans->get();
        $sport_types = SportType::select('sport_id', 'sport')->get();
        $players = Player::select('id', 'vendor_member')->get();

        return view('bet_history_report', [
            'list_boping_trans' => $list_boping_trans,
            'sport_types' => $sport_types,
            'players' => $players
        ]);
    }

    public function winloseReport(Request $request)
    {
        $start_date = $request->query('start_date');
        $end_date = $request->query('end_date');
        $sport_type = $request->query('sport_type');
        $player = $request->query('player');
        
        $sum_winloses = SumWinlose::query();

        if($start_date){
            $sum_winloses->where('winlost_datetime', '>=', $start_date);
        }
        
        if($end_date){
            $sum_winloses->where('winlost_datetime', '<=', $end_date);
        }

        if($sport_type){
            $sum_winloses->where('sport_type', $sport_type);
        }

        if($player){
            $sum_winloses->where('vendor_member_id', $player);
        }

        $sum_winloses->with(['player', 'sport']);

        $sum_winloses = $sum_winloses->get();
        
        $sport_types = SportType::select('sport_id', 'sport')->get();
        $players = Player::select('id', 'vendor_member')->get();

        return view('winlose_report',[
            'sum_winloses' => $sum_winloses,
            'sport_types' => $sport_types,
            'players' => $players
        ]);
    }

    public function winloseReportDetail(Request $request)
    {
        $winlost_datetime = $request->query('winlost_datetime');
        $sport_type = $request->query('sport_type');
        $vendor_member_id = $request->query('vendor_member_id');

        $list_boping_trans = BopingTrans::with(['player', 'sport', 'bet'])
        ->where('winlost_datetime', $winlost_datetime)
        ->where('sport_type', $sport_type)
        ->where('vendor_member_id', $vendor_member_id)
        ->get();

        return view('winlose_report_detail', [
            'list_boping_trans' => $list_boping_trans
        ]);
    }
}
