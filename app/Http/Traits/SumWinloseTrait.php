<?php

namespace App\Http\Traits;

use App\Models\BopingTrans;
use App\Models\Player;
use App\Models\SumWinlose;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait SumWinloseTrait {

    public function storeBopingTrans($bet_details)
    {
        foreach($bet_details as $key => $bet_detail){
            DB::beginTransaction();
            try{           
                //Check if Transaction id exist
                $is_trans_exist = BopingTrans::where('trans_id', $bet_detail['trans_id'])->first();
                if($is_trans_exist){
                    /**
                     * Note: When using job run looping DB Transaction
                     * If you use job (or not probably), make sure close DB transaction first using rollback 
                     * then hit continue; inside looping, 
                     * otherwise it seems the DB transaction not properly closed which cause job attempt 1 on jobs table
                     */
                     
                    DB::rollBack();
                    continue;
                }

                $boping_trans = new BopingTrans();

                $boping_trans->trans_id = $bet_detail['trans_id'];

                $player = Player::where('vendor_member', $bet_detail['vendor_member_id'])->first();
                if(!$player){
                    $player = new Player();
                    $player->vendor_member = $bet_detail['vendor_member_id'];
                    $player->save();
                }
                $boping_trans->vendor_member_id = $player->id;
                $boping_trans->operator_id = $bet_detail['operator_id'];
                $boping_trans->league_id = isset($bet_detail['league_id']) ? $bet_detail['league_id'] : null;
                $boping_trans->leaguename = isset($bet_detail['leaguename']) ? json_encode($bet_detail['leaguename']) : null;
                $boping_trans->match_id = $bet_detail['match_id'];
                $boping_trans->home_id = isset($bet_detail['home_id']) ? $bet_detail['home_id'] : null;
                $boping_trans->hometeamname = isset($bet_detail['hometeamname']) ? json_encode($bet_detail['hometeamname']) : null;
                $boping_trans->away_id = isset($bet_detail['away_id']) ? $bet_detail['away_id'] : null;
                $boping_trans->awayteamname = isset($bet_detail['awayteamname']) ? json_encode($bet_detail['awayteamname']) : null;

                $boping_trans->match_datetime = isset($bet_detail['match_datetime']) ? $bet_detail['match_datetime'] : null;
                $boping_trans->sport_type = $bet_detail['sport_type'];
                $boping_trans->bet_type = $bet_detail['bet_type'];
                $boping_trans->parlay_ref_no = isset($bet_detail['parlay_ref_no']) ? $bet_detail['parlay_ref_no'] : null;
                $boping_trans->odds = $bet_detail['odds'];
                $boping_trans->stake = $bet_detail['stake'];
                $boping_trans->transaction_time = $bet_detail['transaction_time'];
                $boping_trans->ticket_status = $bet_detail['ticket_status'];
                $boping_trans->winlost_amount = $bet_detail['winlost_amount'];
                $boping_trans->after_amount = isset($bet_detail['after_amount']) ? $bet_detail['after_amount'] : 0;

                $boping_trans->currency = $bet_detail['currency'];
                $boping_trans->winlost_datetime = $bet_detail['winlost_datetime'];
                $boping_trans->odds_type = $bet_detail['odds_type'];
                $boping_trans->bet_team = isset($bet_detail['bet_team']) ?$bet_detail['bet_team'] : '';
                $boping_trans->is_lucky = isset($bet_detail['isLucky']) ? $bet_detail['isLucky'] : null;
                $boping_trans->home_hdp = isset($bet_detail['home_hdp']) ? $bet_detail['home_hdp'] : null;
                $boping_trans->away_hdp = isset($bet_detail['away_hdp']) ? $bet_detail['away_hdp'] : null;
                $boping_trans->hdp = isset($bet_detail['hdp']) ? $bet_detail['hdp'] : null;
                $boping_trans->betfrom = $bet_detail['betfrom'];
                $boping_trans->islive = $bet_detail['islive'];

                $boping_trans->home_score = isset($bet_detail['home_score']) ? $bet_detail['home_score'] : null;
                $boping_trans->away_score = isset($bet_detail['away_score']) ? $bet_detail['away_score'] : null;
                $boping_trans->settlement_time = isset($bet_detail['settlement_time']) ? $bet_detail['settlement_time'] : null;
                $boping_trans->custominfo1 = $bet_detail['customInfo1'];
                $boping_trans->custominfo2 = $bet_detail['customInfo2'];
                $boping_trans->custominfo3 = $bet_detail['customInfo3'];
                $boping_trans->custominfo4 = $bet_detail['customInfo4'];
                $boping_trans->custominfo5 = $bet_detail['customInfo5'];
                $boping_trans->ba_status = $bet_detail['ba_status'];
                $boping_trans->version_key = $bet_detail['version_key'];
                
                $boping_trans->parlaydata = isset($bet_detail['ParlayData']) ? json_encode($bet_detail['ParlayData']) : null;

                // Bet Number Details
                $boping_trans->last_ball_no = isset($bet_detail['last_ball_no']) ? $bet_detail['last_ball_no']: null;

                $boping_trans->save();

                // Insert to sum_winlose
                $this->recalculateSumWinlose($boping_trans); 
                DB::commit();
            }catch (Exception $e) {
                Log::debug($e->getMessage());
                DB::rollBack();
            }
        }
    }
    public function recalculateSumWinlose($boping_trans) {
        $sum_winlose = SumWinlose::where('winlost_datetime', $boping_trans->winlost_datetime)
        ->where('sport_type', $boping_trans->sport_type)
        ->where('vendor_member_id', $boping_trans->vendor_member_id)
        ->first();

        if($sum_winlose){
            $sum_winlose->turnover_amount += $boping_trans->stake;
            if($boping_trans->winlost_amount > 0){
                $sum_winlose->win += $boping_trans->winlost_amount;
            }
            if($boping_trans->winlost_amount < 0){
                $sum_winlose->lose += abs($boping_trans->winlost_amount);
            }

            $sum_winlose->winlose = $sum_winlose->win - $sum_winlose->lose;
            $sum_winlose->save();
        }else{
            $new_sum_winlose = new SumWinlose();
            $new_sum_winlose->turnover_amount = $boping_trans->stake;
            $new_sum_winlose->winlost_datetime = $boping_trans->winlost_datetime;
            $new_sum_winlose->sport_type = $boping_trans->sport_type;
            $new_sum_winlose->vendor_member_id = $boping_trans->vendor_member_id;
            if($boping_trans->winlost_amount > 0){
                $new_sum_winlose->win = $boping_trans->winlost_amount;
            }
            if($boping_trans->winlost_amount < 0){
                $new_sum_winlose->lose = abs($boping_trans->winlost_amount);
            }

            $new_sum_winlose->winlose = $new_sum_winlose->win - $new_sum_winlose->lose;
            $new_sum_winlose->save();
        }
    }
}