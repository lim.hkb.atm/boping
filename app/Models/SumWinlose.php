<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SumWinlose extends Model
{
    use HasFactory;

    protected $table = 'sum_winlose';

    public function player()
    {
        return $this->belongsTo(Player::class, 'vendor_member_id', 'id');
    }

    public function sport()
    {
        return $this->belongsTo(SportType::class, 'sport_type', 'sport_id');
    }
}
