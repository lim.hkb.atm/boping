<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BopingTrans extends Model
{
    use HasFactory;

    protected $table = 'boping_trans';
    public $timestamps = false;

    public function player()
    {
        return $this->belongsTo(Player::class, 'vendor_member_id', 'id');
    }

    public function sport()
    {
        return $this->belongsTo(SportType::class, 'sport_type', 'sport_id');
    }

    public function bet()
    {
        return $this->belongsTo(BetType::class, 'bet_type', 'bet_type_id');
    }

}
