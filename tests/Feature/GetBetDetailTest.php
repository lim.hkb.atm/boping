<?php

namespace Tests\Feature;

use App\Jobs\ProcessBopingTrans;
use Illuminate\Console\Scheduling\Event;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Http;

use Tests\TestCase;

class GetBetDetailTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use RefreshDatabase;

    /** @test */
    public function can_not_get_bet_details_with_incorrect_vendor_id()
    {
        $response = Http::asForm()->post('http://x9f2tsa.bw6688.com/api/GetBetDetail/', [
            'vendor_id' => '0dfz5qetx'
        ]);
        
        // 401 = Unauthorized
        $this->assertEquals(401, $response->status());
    }

    /** @test */
    public function can_get_bet_details_with_correct_vendor_id()
    {
        $response = Http::asForm()->post('http://x9f2tsa.bw6688.com/api/GetBetDetail/', [
            'vendor_id' => '0dfz5qetxt'
        ]);
        
        $this->assertEquals(200, $response->status());
    }

    /** @test */
    public function can_dispatch_process_boping_trans_job()
    {
        Bus::fake();
        $this->get('/get-bet');
        Bus::assertDispatched(ProcessBopingTrans::class);
    }
}
