<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/get-bet', [App\Http\Controllers\GetBetDetailsController::class, 'index']);
Route::get('/bet-history-report', [App\Http\Controllers\GetBetDetailsController::class, 'betHistoryReport']);
Route::get('/winlose-report', [App\Http\Controllers\GetBetDetailsController::class, 'winloseReport']);
Route::get('/winlose-report-detail', [App\Http\Controllers\GetBetDetailsController::class, 'winloseReportDetail']);