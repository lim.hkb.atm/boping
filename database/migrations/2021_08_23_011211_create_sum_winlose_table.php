<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSumWinloseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sum_winlose', function (Blueprint $table) {
            $table->id();
            $table->dateTime('winlost_datetime');
            $table->unsignedTinyInteger('sport_type')->nullable();
            $table->unsignedBigInteger('vendor_member_id');
            $table->decimal('turnover_amount', $precision = 8, $scale = 2);
            $table->decimal('win', $precision = 8, $scale = 2)->default(0);
            $table->decimal('lose', $precision = 8, $scale = 2)->default(0);
            $table->decimal('winlose', $precision = 8, $scale = 2);
            $table->timestamps();
        });

        Schema::table('sum_winlose', function (Blueprint $table) {
            $table->foreign('vendor_member_id')->references('id')->on('player');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boping_trans', function (Blueprint $table) {
            $table->dropForeign(['vendor_member_id']);
        });

        Schema::dropIfExists('sum_winlose');
    }
}
