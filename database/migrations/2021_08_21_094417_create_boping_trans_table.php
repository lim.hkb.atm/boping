<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBopingTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boping_trans', function (Blueprint $table) {
            $table->id();

            $table->string('trans_id');
            $table->unsignedBigInteger('vendor_member_id');
            $table->string('operator_id', 50);
            $table->integer('league_id')->nullable();
            $table->json('leaguename')->nullable();
            $table->integer('match_id');
            $table->integer('home_id')->nullable();
            $table->longText('hometeamname')->nullable();
            $table->integer('away_id')->nullable();
            $table->longText('awayteamname')->nullable();

            $table->dateTime('match_datetime')->nullable();
            $table->unsignedTinyInteger('sport_type')->nullable();
            $table->unsignedBigInteger('bet_type');
            $table->bigInteger('parlay_ref_no')->nullable();
            $table->decimal('odds', $precision = 8, $scale = 2);
            $table->decimal('stake', $precision = 8, $scale = 2);
            $table->dateTime('transaction_time');
            $table->string('ticket_status', 15);
            $table->decimal('winlost_amount', $precision = 8, $scale = 2);
            $table->decimal('after_amount', $precision = 8, $scale = 2);

            $table->tinyInteger('currency');
            $table->dateTime('winlost_datetime');
            $table->tinyInteger('odds_type');
            $table->string('bet_team', 100);
            $table->string('is_lucky', 5)->nullable();
            $table->decimal('home_hdp', $precision = 8, $scale = 2)->nullable();
            $table->decimal('away_hdp', $precision = 8, $scale = 2)->nullable();
            $table->decimal('hdp', $precision = 8, $scale = 2)->nullable();
            $table->string('betfrom', 5);
            $table->string('islive', 1);

            $table->smallInteger('home_score')->nullable();
            $table->smallInteger('away_score')->nullable();
            $table->dateTime('settlement_time')->nullable();
            $table->string('custominfo1', 200);
            $table->string('custominfo2', 200);
            $table->string('custominfo3', 200);
            $table->string('custominfo4', 200);
            $table->string('custominfo5', 200);
            $table->string('ba_status', 1);
            $table->bigInteger('version_key')->unique();

            $table->longText('parlaydata')->nullable();

            $table->bigInteger('last_ball_no')->nullable();
        });

        Schema::table('boping_trans', function (Blueprint $table) {
            $table->foreign('vendor_member_id')->references('id')->on('player');
            $table->foreign('bet_type')->references('bet_type_id')->on('bet_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boping_trans', function (Blueprint $table) {
            $table->dropForeign(['vendor_member_id']);
            $table->dropForeign(['bet_type']);
        });
        

        Schema::dropIfExists('boping_trans');
    }
}
