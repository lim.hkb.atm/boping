sport_type
==========

| sport_id |                   sport                   |   sport_group    |
|----------|-------------------------------------------|------------------|
| 1        | Soccer                                    | SportBooks       |
| 2        | Basketball                                | SportBooks       |
| 3        | Football                                  | SportBooks       |
| 4        | Ice Hockey                                | SportBooks       |
| 5        | Tennis                                    | SportBooks       |
| 6        | Volleyball                                | SportBooks       |
| 7        | Snooker/Pool                              | SportBooks       |
| 8        | Baseball                                  | SportBooks       |
| 9        | Badminton                                 | SportBooks       |
| 10       | Golf                                      | SportBooks       |
| 11       | Motorsports                               | SportBooks       |
| 12       | Swimming                                  | SportBooks       |
| 13       | Politics                                  | SportBooks       |
| 14       | Water Polo                                | SportBooks       |
| 15       | Diving                                    | SportBooks       |
| 16       | Boxing/MMA                                | SportBooks       |
| 17       | Archery                                   | SportBooks       |
| 18       | Table Tennis                              | SportBooks       |
| 19       | Weightlifting                             | SportBooks       |
| 20       | Canoeing                                  | SportBooks       |
| 21       | Gymnastics                                | SportBooks       |
| 22       | Athletics                                 | SportBooks       |
| 23       | Equestrian                                | SportBooks       |
| 24       | Handball                                  | SportBooks       |
| 25       | Darts                                     | SportBooks       |
| 26       | Rugby                                     | SportBooks       |
| 28       | Field Hockey                              | SportBooks       |
| 29       | Winter Sports                             | SportBooks       |
| 30       | Squash                                    | SportBooks       |
| 31       | Entertainment                             | SportBooks       |
| 32       | Netball                                   | SportBooks       |
| 33       | Cycling                                   | SportBooks       |
| 34       | Fencing                                   | SportBooks       |
| 35       | Judo                                      | SportBooks       |
| 36       | M. Pentathlon                             | SportBooks       |
| 37       | Rowing                                    | SportBooks       |
| 38       | Sailing                                   | SportBooks       |
| 39       | Shooting                                  | SportBooks       |
| 40       | Taekwondo                                 | SportBooks       |
| 41       | Triathlon                                 | SportBooks       |
| 42       | Wrestling                                 | SportBooks       |
| 43       | E-Sports                                  | SportBooks       |
| 44       | Muay Thai                                 | SportBooks       |
| 45       | Beach Volleyball                          | SportBooks       |
| 47       | Kabaddi                                   | SportBooks       |
| 48       | Sepak Takraw                              | SportBooks       |
| 49       | Futsal                                    | SportBooks       |
| 50       | Cricket                                   | SportBooks       |
| 51       | Beach Soccer                              | SportBooks       |
| 52       | Poker                                     | SportBooks       |
| 53       | Chess                                     | SportBooks       |
| 54       | Olympics                                  | SportBooks       |
| 55       | Finance                                   | SportBooks       |
| 56       | Lotto                                     | SportBooks       |
| 99       | Other Sports                              | SportBooks       |
| 154      | HorseRacing FixedOdds                     | Racing           |
| 161      | Number Game                               | Number Game      |
| 162      | Live Casino                               | Live Casino      |
| 164      | Happy 5                                   | Number Game      |
| 165      | Arcadia Gaming                            | Casino           |
| 167      | CG                                        | Casino           |
| 168      | Happy 5 Linked Games                      | Number Game      |
| 169      | Sports Lottery                            | Sports lottery   |
| 171      | Vgaming                                   | Casino           |
| 180      | Virtual Soccer                            | Virtual Sport    |
| 181      | Virtual Horse Racing (temporarily closed) | Virtual Sport    |
| 182      | Virtual Greyhound (temporarily closed)    | Virtual Sport    |
| 183      | Virtual Speedway (temporarily closed)     | Virtual Sport    |
| 184      | Virtual Motorsports (temporarily closed)  | Virtual Sport    |
| 185      | Virtual Cycling (temporarily closed)      | Virtual Sport    |
| 186      | Virtual Tennis                            | Virtual Sport    |
| 190      | Virtual Soccer League                     | Virtual Sports 2 |
| 191      | Virtual Soccer Nation                     | Virtual Sports 2 |
| 192      | Virtual Soccer World Cup                  | Virtual Sports 2 |
| 193      | Virtual Basketball                        | Virtual Sports 2 |
| 194      | Virtual Soccer Asian Cup                  | Virtual Sports 2 |
| 195      | Virtual Soccer English Premier            | Virtual Sports 2 |
| 196      | Virtual Soccer Champions Cup              | Virtual Sports 2 |
| 197      | Virtual Soccer Euro Cup                   | Virtual Sports 2 |
| 199      | Virtual Sports Parlay                     | Virtual Sports 2 |
| 200      | Saba.club                                 | Casino           |
| 202      | RNG Keno                                  | RNG KENO         |
| 204      | Colossus Bet                              | SportBooks       |
| 208      | RNG – RNG Game                          | Casino           |
| 209      | RNG – Mini Games                        | Casino           |
| 210      | RNG – Mobile                            | Casino           |
| 211      | Allbet                                    | Live Casino      |
| 212      | Macau Games                               | Casino           |
| 219      | Fishing World                             | Casino           |
| 220      | Lottery                                   | RNG KENO         |
| 222      | Table Game                                | RNG KENO         |
| 223      | Togel 4D                                  | RNG KENO         |
| 232      | PG Soft                                   | Casino           |
| 234      | Joker                                     | Casino           |
| 236      | GDG                                       | Live Casino      |
| 243      | AE Sexy                                   | Live Casino      |
| 244      | SA Gaming                                 | Live Casino      |
| 245      | Virtual Games                             | Casino           |
| 247      | SG                                        | Casino           |
| 248      | Pragmatic Play                            | Casino           |
(100 rows)

