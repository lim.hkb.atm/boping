bet_type
========

| bet_type_id |                                                    bet_type_name                                                    |
|-------------|---------------------------------------------------------------------------------------------------------------------|
| 1           | Handicap                                                                                                            |
| 2           | Odd/Even                                                                                                            |
| 3           | Over/Under                                                                                                          |
| 4           | Correct Score (Soccer)                                                                                              |
| 5           | FT.1X2                                                                                                              |
| 6           | Total Goal (Soccer)                                                                                                 |
| 7           | 1H Handicap                                                                                                         |
| 8           | 1H Over/Under                                                                                                       |
| 10          | Outright                                                                                                            |
| 11          | Total Corners (Soccer)                                                                                              |
| 12          | 1H Odd/Even                                                                                                         |
| 13          | Clean Sheet (Soccer)                                                                                                |
| 14          | First Goal/Last Goal (Soccer)                                                                                       |
| 15          | 1H 1X2                                                                                                              |
| 16          | HT/FT (Soccer)                                                                                                      |
| 17          | 2H Handicap (Soccer)                                                                                                |
| 18          | 2H Over/Under (Soccer)                                                                                              |
| 19          | Substitutes                                                                                                         |
| 20          | Moneyline                                                                                                           |
| 21          | 1H Moneyline                                                                                                        |
| 22          | Next Goal (Soccer)                                                                                                  |
| 23          | Next Corner                                                                                                         |
| 24          | Double Chance (Soccer)                                                                                              |
| 25          | Draw No Bet (Soccer)                                                                                                |
| 26          | Both/One/Neither Team To Score (Soccer)                                                                             |
| 27          | To Win To Nil (Soccer)                                                                                              |
| 28          | 3-Way Handicap (Soccer)                                                                                             |
| 29          | System Parlay (Other Sports)                                                                                        |
| 30          | 1H Correct Score (Soccer)                                                                                           |
| 71          | Casino Games (Casino)                                                                                               |
| 81          | 1st Ball O/U (Number Game)                                                                                          |
| 82          | Last Ball O/U (Number Game)                                                                                         |
| 83          | 1st Ball O/E (Number Game)                                                                                          |
| 84          | Last Ball O/E (Number Game)                                                                                         |
| 85          | Next Over/Under (Number Game)                                                                                       |
| 86          | Next Odd/Even (Number Game)                                                                                         |
| 89          | Next Combo (Number Game)                                                                                            |
| 90          | Number wheel (Number Game)                                                                                          |
| 91          | Next R/B(Number Game)                                                                                               |
| 121         | Home No Bet (Soccer)                                                                                                |
| 122         | Away No Bet (Soccer)                                                                                                |
| 123         | Draw/No Draw (Soccer)                                                                                               |
| 124         | FT.1X2 HDP (Soccer)                                                                                                 |
| 125         | 1H 1X2 HDP (Soccer)                                                                                                 |
| 126         | 1H Total Goal (Soccer)                                                                                              |
| 127         | 1H First Goal/Last Goal (Soccer)                                                                                    |
| 128         | HT/FT Odd/Even (Soccer)                                                                                             |
| 133         | Home To Win Both Halves (Soccer)                                                                                    |
| 438         | Home To Win Both Halves (Soccer)                                                                                    |
| 134         | Away To Win Both Halves (Soccer)                                                                                    |
| 439         | Away To Win Both Halves (Soccer)                                                                                    |
| 135         | Penalty Shootout (Soccer)                                                                                           |
| 140         | Highest Scoring Half (Soccer)                                                                                       |
| 442         | Highest Scoring Half (Soccer)                                                                                       |
| 141         | Highest Scoring Half Home Team (Soccer)                                                                             |
| 443         | Highest Scoring Half Home Team (Soccer)                                                                             |
| 142         | Highest Scoring Half Away Team (Soccer)                                                                             |
| 444         | Highest Scoring Half Away Team (Soccer)                                                                             |
| 145         | Both Teams To Score (Soccer)                                                                                        |
| 146         | 2H Both Teams To Score (Soccer)                                                                                     |
| 433         | 2H Both Teams To Score (Soccer)                                                                                     |
| 147         | Home To Score In Both Halves (Soccer)                                                                               |
| 436         | Home To Score In Both Halves (Soccer)                                                                               |
| 148         | Away To Score In Both Halves (Soccer)                                                                               |
| 437         | Away To Score In Both Halves (Soccer)                                                                               |
| 149         | Home To Win Either Half (Soccer)                                                                                    |
| 440         | Home To Win Either Half (Soccer)                                                                                    |
| 150         | Away To Win Either Half (Soccer)                                                                                    |
| 441         | Away To Win Either Half (Soccer)                                                                                    |
| 151         | 1H Double Chance (Soccer)                                                                                           |
| 410         | 1H Double Chance (Soccer)                                                                                           |
| 152         | Half Time/Full Time Correct Score (Soccer)                                                                          |
| 416         | Half Time/Full Time Correct Score (Soccer)                                                                          |
| 153         | Game HDP (Tennis)                                                                                                   |
| 154         | Set x Winner (Tennis)                                                                                               |
| 155         | Set x Game Handicap (Tennis)                                                                                        |
| 156         | Set x Total Game Over/Under (Tennis)                                                                                |
| 1301        | Match Winner (Tennis)                                                                                               |
| 1302        | Match Correct Score (Tennis)                                                                                        |
| 1303        | Set Handicap (Tennis)                                                                                               |
| 1305        | Match Total Games Odd/Even (Tennis)                                                                                 |
| 1306        | Match Total Games Over/under (Tennis)                                                                               |
| 1308        | Match Games Handicap (Tennis)                                                                                       |
| 1311        | Set x Winner (Tennis)                                                                                               |
| 1312        | Set x Total Games (Tennis)                                                                                          |
| 1316        | Set x Game Handicap (Tennis)                                                                                        |
| 1317        | Set x Correct Score (Tennis)                                                                                        |
| 1318        | Set x Total Game Odd/Even (Tennis)                                                                                  |
| 1324        | Set x Game y Winner (Tennis)                                                                                        |
| 157         | Odd/Even (Soccer)                                                                                                   |
| 159         | Exact Total Goals (Soccer)                                                                                          |
| 406         | Exact Total Goals (Soccer)                                                                                          |
| 160         | Next Goal (Soccer)                                                                                                  |
| 161         | Exact Home Team Goals (Soccer)                                                                                      |
| 407         | Exact Home Team Goals (Soccer)                                                                                      |
| 162         | Exact Away Team Goals (Soccer)                                                                                      |
| 409         | Exact Away Team Goals (Soccer)                                                                                      |
| 163         | Result/Total Goals (Soccer)                                                                                         |
| 144         | Result/Total Goals (Soccer)                                                                                         |
| 164         | Extra Time Next Goal (Soccer)                                                                                       |
| 165         | Extra Time 1H Correct Score (Soccer)                                                                                |
| 166         | Extra Time Correct Score (Soccer)                                                                                   |
| 167         | Extra Time 1H 1X2 (Soccer)                                                                                          |
| 168         | Who Advances To Next Round (Soccer)                                                                                 |
| 169         | Next Goal Time (Soccer)                                                                                             |
| 170         | Teams To Score (Soccer)                                                                                             |
| 171         | Winning Margin (Soccer)                                                                                             |
| 408         | Winning Margin (Soccer)                                                                                             |
| 172         | Result And First Team To Score (Soccer)                                                                             |
| 415         | Result And First Team To Score (Soccer)                                                                             |
| 173         | Extra Time Yes/No (Soccer)                                                                                          |
| 174         | Extra Time And Goal (Soccer)                                                                                        |
| 175         | Match Decided Method (Soccer)                                                                                       |
| 176         | First Ten Minutes 1X2 (Soccer)                                                                                      |
| 177         | 2H 1x2 (Soccer)                                                                                                     |
| 430         | 2H 1x2 (Soccer)                                                                                                     |
| 178         | 2H Over/Under (Soccer)                                                                                              |
| 179         | Exact 1H Goals (Soccer)                                                                                             |
| 180         | 1H Next Goal (Soccer)                                                                                               |
| 181         | 1H Exact Home Team Goals (Soccer)                                                                                   |
| 182         | 1H Exact Away Team Goals (Soccer)                                                                                   |
| 184         | 2H Odd/Even (Soccer)                                                                                                |
| 428         | 2H Odd/Even (Soccer)                                                                                                |
| 185         | 2H Draw No Bet (Soccer)                                                                                             |
| 432         | 2H Draw No Bet (Soccer)                                                                                             |
| 186         | 2H Double Chance (Soccer)                                                                                           |
| 431         | 2H Double Chance (Soccer)                                                                                           |
| 187         | Exact 2H Goals (Soccer)                                                                                             |
| 188         | 1H Both Teams To Score (Soccer)                                                                                     |
| 427         | 1H Both Teams To Score (Soccer)                                                                                     |
| 189         | Both Halves Over 1.5 Yes/No (Soccer)                                                                                |
| 434         | Both Halves Over 1.5 Yes/No (Soccer)                                                                                |
| 190         | Both Halves Under 1.5 Yes/No (Soccer)                                                                               |
| 435         | Both Halves Under 1.5 Yes/No (Soccer)                                                                               |
| 191         | 1H Draw No Bet (Soccer)                                                                                             |
| 411         | 1H Draw No Bet (Soccer)                                                                                             |
| 192         | First Goal Specific Time (10 min) (Soccer)                                                                          |
| 193         | First Goal Specific Time (15 min) (Soccer)                                                                          |
| 194         | Corners Odd/Even (Soccer)                                                                                           |
| 195         | Home Team Exact Corners (Soccer)                                                                                    |
| 196         | Away Team Exact Corners (Soccer)                                                                                    |
| 197         | Home Team Total Corners Over/Under (Soccer)                                                                         |
| 198         | Away Team Total Corners Over/Under (Soccer)                                                                         |
| 199         | Total Corners (Soccer)                                                                                              |
| 200         | 1H Home Team Exact Corners (Soccer)                                                                                 |
| 201         | 1H Away Team Exact Corners (Soccer)                                                                                 |
| 202         | 1H Total Corners (Soccer)                                                                                           |
| 203         | 1H Corners Odd/Even (Soccer)                                                                                        |
| 204         | 1H Home Corner Over/Under (Soccer)                                                                                  |
| 205         | 1H Away Corner Over/Under (Soccer)                                                                                  |
| 206         | First Corner (Soccer)                                                                                               |
| 207         | 1H First Corner (Soccer)                                                                                            |
| 208         | Last Corner (Soccer)                                                                                                |
| 209         | 1H Last Corner (Soccer)                                                                                             |
| 210         | Player Sent Off (Soccer)                                                                                            |
| 211         | 1H Player Sent off (Soccer)                                                                                         |
| 212         | Home Team Player Sent Off (Soccer)                                                                                  |
| 213         | 1H Home Team Player Sent Off (Soccer)                                                                               |
| 214         | Away Team Player Sent Off (Soccer)                                                                                  |
| 215         | 1H Away Team Player Sent Off (Soccer)                                                                               |
| 221         | Next 1 Minute (Soccer)                                                                                              |
| 222         | Next 5 Minutes (Soccer)                                                                                             |
| 223         | What will happen first in next 1 minute (Soccer)                                                                    |
| 224         | What will happen first in next 5 minutes (Soccer)                                                                   |
| 225         | Next 1 Minute Set Piece (Soccer)                                                                                    |
| 226         | Which combination will happen first in next 1 min (Soccer)                                                          |
| 227         | Which combination will happen first in next 5 mins (Soccer)                                                         |
| 228         | SABA OU Time Machine (Soccer)                                                                                       |
| 229         | SABA HDP Time Machine (Soccer)                                                                                      |
| 2101        | Baccarat (RNG – RNG Game)                                                                                         |
| 2102        | Battle War (RNG – RNG Game)                                                                                       |
| 2103        | Blackjack (RNG – RNG Game)                                                                                        |
| 2104        | Poker Tri (RNG – RNG Game)                                                                                        |
| 2105        | Casino Hold’em (RNG – RNG Game)                                                                                 |
| 2106        | Pai Gow Poker (RNG – RNG Game)                                                                                    |
| 2107        | Mini Roulette (RNG – RNG Game)                                                                                    |
| 2108        | Roulette Pro (RNG – RNG Game)                                                                                     |
| 2109        | Sic Bo (RNG – RNG Game)                                                                                           |
| 2110        | Jacks or Better (RNG – RNG Game)                                                                                  |
| 2111        | Roulette Pro (Fast) (RNG – RNG Game)                                                                              |
| 2112        | Blackjack Perfect Pair (RNG – RNG Game)                                                                           |
| 2113        | Baccarat Super 6 (RNG – RNG Game)                                                                                 |
| 2114        | Fish Prawn Crab (RNG – RNG Game)                                                                                  |
| 2115        | Dragon Tiger (RNG – RNG Game)                                                                                     |
| 2116        | Three Knights (RNG – RNG Game)                                                                                    |
| 2117        | Money Wheel (RNG – RNG Game)                                                                                      |
| 2118        | Fan Tan (RNG – RNG Game)                                                                                          |
| 2119        | Card Wheel (RNG – RNG Game)                                                                                       |
| 2120        | Texas Hold’em (RNG – RNG Game)                                                                                  |
| 2121        | Face Up 21 (RNG – RNG Game)                                                                                       |
| 2122        | Three Cards Rummy (RNG – RNG Game)                                                                                |
| 2123        | Red Dog (RNG – RNG Game)                                                                                          |
| 2124        | Multi Wheel Roulette (RNG – RNG Game)                                                                             |
| 2125        | Monkey HiLo (RNG – RNG Game)                                                                                      |
| 2126        | Mahjong HiLo (RNG – RNG Game)                                                                                     |
| 2127        | Keno Camera (RNG – RNG Game)                                                                                      |
| 2128        | Keno Chinese (RNG – RNG Game)                                                                                     |
| 2129        | Turf King (RNG – RNG Game)                                                                                        |
| 2130        | SpaKz (RNG – RNG Game)                                                                                            |
| 2131        | Maya Push (RNG – RNG Game)                                                                                        |
| 2132        | Gong Xi Fa Chai (RNG – RNG Game)                                                                                  |
| 2133        | Love Story (RNG – RNG Game)                                                                                       |
| 2134        | Penalty Hat-Trick (RNG – RNG Game)                                                                                |
| 2135        | Fruit Scratch (RNG – RNG Game)                                                                                    |
| 2136        | Ho Ho Ho (RNG – RNG Game)                                                                                         |
| 2137        | Knock2 Eggs (RNG – RNG Game)                                                                                      |
| 2138        | Poke Mole (RNG – RNG Game)                                                                                        |
| 2139        | Fancy Scratch (RNG – RNG Game)                                                                                    |
| 2140        | Flames of Olympics (RNG – RNG Game)                                                                               |
| 2141        | Sushi Bar (RNG – RNG Game)                                                                                        |
| 2142        | Baseball Battle (RNG – RNG Game)                                                                                  |
| 2143        | 10’s or Better (RNG – RNG Game)                                                                                 |
| 2144        | 4-Line Jacks or Better (RNG – RNG Game)                                                                           |
| 2145        | 25-Line Jacks or Better (RNG – RNG Game)                                                                          |
| 2146        | 50-Line Jacks or Better (RNG – RNG Game)                                                                          |
| 2147        | Jungle Wild (RNG – RNG Game)                                                                                      |
| 2148        | Engagement (RNG – RNG Game)                                                                                       |
| 2149        | God of Prosperity (RNG – RNG Game)                                                                                |
| 2150        | Money Slot (RNG – RNG Game)                                                                                       |
| 2151        | Fox Force 5 (RNG – RNG Game)                                                                                      |
| 2152        | Journey to the West (RNG – RNG Game)                                                                              |
| 2153        | Winter Slot (RNG – RNG Game)                                                                                      |
| 2154        | Western Zodiac (RNG – RNG Game)                                                                                   |
| 2155        | Chinese Zodiac (RNG – RNG Game)                                                                                   |
| 2156        | Water Margin (RNG – RNG Game)                                                                                     |
| 2157        | Champions (RNG – RNG Game)                                                                                        |
| 2158        | Crazy Hospital (RNG – RNG Game)                                                                                   |
| 2159        | Mahjong Legend (RNG – RNG Game)                                                                                   |
| 2160        | Halloween (RNG – RNG Game)                                                                                        |
| 2161        | Happy Vampire (RNG – RNG Game)                                                                                    |
| 2162        | Football Fans (RNG – RNG Game)                                                                                    |
| 2163        | Oriental Beauty (RNG – RNG Game)                                                                                  |
| 2164        | Ninja Hero (RNG – RNG Game)                                                                                       |
| 2165        | Tales of Brothel (RNG – RNG Game)                                                                                 |
| 2166        | Fortune Gods (RNG – RNG Game)                                                                                     |
| 2167        | Magician (RNG – RNG Game)                                                                                         |
| 2168        | GOP(both way) (RNG – RNG Game)                                                                                    |
| 2169        | Iron Fist (RNG – RNG Game)                                                                                        |
| 2170        | Jungle Wild(Both Way) (RNG – RNG Game)                                                                            |
| 2171        | Dragon Slot (RNG – RNG Game)                                                                                      |
| 2172        | Cowboy Fever (RNG – RNG Game)                                                                                     |
| 2173        | Dino Camp (RNG – RNG Game)                                                                                        |
| 2174        | Tattoo Art (RNG – RNG Game)                                                                                       |
| 2175        | Mamak Corner (RNG – RNG Game)                                                                                     |
| 2176        | Sea Orchestra (RNG – RNG Game)                                                                                    |
| 2177        | Cash Out (RNG – RNG Game)                                                                                         |
| 2180        | Jacks or Better (RNG – RNG Game)                                                                                  |
| 2201        | Baccarat (RNG – Mini Game)                                                                                        |
| 2202        | Casino Hold’em (RNG – Mini Game)                                                                                |
| 2203        | BlackJack (RNG – Mini Game)                                                                                       |
| 2204        | Jacks or Better (RNG – Mini Game)                                                                                 |
| 2205        | Sic Bo (RNG – Mini Game)                                                                                          |
| 2206        | Money Slot (RNG – Mini Game)                                                                                      |
| 2208        | Jacks or Better (RNG – Mini Game)                                                                                 |
| 2301        | Baccarat (RNG – Mobile)                                                                                           |
| 2302        | Casino Hold’ em (RNG – Mobile)                                                                                  |
| 2303        | Blackjack (RNG – Mobile)                                                                                          |
| 2304        | Jungle Wild (RNG – Mobile)                                                                                        |
| 2305        | God of Prosperity (RNG – Mobile)                                                                                  |
| 2306        | Sic Bo (RNG – Mobile)                                                                                             |
| 2307        | Oriental Beauty (RNG – Mobile)                                                                                    |
| 2308        | Magician (RNG – Mobile)                                                                                           |
| 2309        | Tales of Brothel (RNG – Mobile)                                                                                   |
| 2310        | Mini Roulette (RNG – Mobile)                                                                                      |
| 2311        | Baccarat Super 6 (RNG – Mobile)                                                                                   |
| 2312        | Blackjack Perfect Pair (RNG – Mobile)                                                                             |
| 2313        | Fabulous 4 Baccarat (RNG – Mobile)                                                                                |
| 2314        | Face Up 21 (RNG – Mobile)                                                                                         |
| 2315        | Tri Card Poker(RNG – Mobile)                                                                                      |
| 2316        | Dragon Tiger(RNG – Mobile)                                                                                        |
| 2317        | Texas Hold’em(RNG – Mobile)                                                                                     |
| 2318        | Pai Gow Poker(RNG – Mobile)                                                                                       |
| 2319        | Baccarat (Progressive JP) – Main Game (RNG – Mobile)                                                            |
| 2320        | Baccarat (Progressive JP) – Jackpot Game (RNG – Mobile)                                                         |
| 2321        | Roulette Pro(RNG – Mobile)                                                                                        |
| 2901        | Baccarat (RNG –Tablet)                                                                                            |
| 2902        | Casino Hold’ em (RNG –Tablet)                                                                                   |
| 2903        | Blackjack (RNG –Tablet)                                                                                           |
| 2904        | Jungle Wild (RNG –Tablet)                                                                                         |
| 2905        | God of Prosperity (RNG –Tablet)                                                                                   |
| 2906        | Sic Bo (RNG –Tablet)                                                                                              |
| 2907        | Oriental Beauty (RNG –Tablet)                                                                                     |
| 2908        | Magician (RNG –Tablet)                                                                                            |
| 2909        | Tales of Brothel (RNG –Tablet)                                                                                    |
| 2910        | Mini Roulette (RNG –Tablet)                                                                                       |
| 2911        | Baccarat Super 6 (RNG –Tablet)                                                                                    |
| 2912        | Blackjack(Perfect Pair) (RNG –Tablet)                                                                             |
| 2913        | Face Up 21 (RNG –Tablet)                                                                                          |
| 2914        | Tri Card Poker (RNG –Tablet)                                                                                      |
| 2915        | Dragon Tiger (RNG –Tablet)                                                                                        |
| 2916        | Texas Hold’em (RNG –Tablet)                                                                                     |
| 2917        | Pai Gow Poker (RNG –Tablet)                                                                                       |
| 2919        | Baccarat (Progressive JP) – Main Game (RNG –Tablet)                                                             |
| 2920        | Baccarat (Progressive JP) – Jackpot Game (RNG –Tablet)                                                          |
| 2921        | Roulette Pro (RNG –Tablet)                                                                                        |
| 501         | Match Winner (Cricket)                                                                                              |
| 401         | Home Team Over/Under (Basketball,Dec)                                                                               |
| 402         | Away Team Over/Under (Basketball,Dec)                                                                               |
| 403         | 1H Home Team Over/Under (Basketball,Dec)                                                                            |
| 404         | 1H Away Team Over/Under (Basketball,Dec)                                                                            |
| 405         | 2H Correct Score (Soccer)                                                                                           |
| 412         | Exact 1H Goals (Soccer)                                                                                             |
| 413         | Correct Score (Soccer)                                                                                              |
| 414         | 1H Correct Score (Soccer)                                                                                           |
| 417         | Both Teams To Score/Result (Soccer)                                                                                 |
| 418         | Both Teams To Score/Total Goals (Soccer)                                                                            |
| 419         | Which Half First Goal (Soccer)                                                                                      |
| 420         | Home Team Which Half First Goal (Soccer)                                                                            |
| 421         | Away Team Which Half First Goal (Soccer)                                                                            |
| 422         | First Team 2 Goals (Soccer)                                                                                         |
| 423         | First Team 3 Goals (Soccer)                                                                                         |
| 424         | First Goal Method (Soccer)                                                                                          |
| 425         | To Win From Behind (Soccer)                                                                                         |
| 426         | 1H Winning Margin (Soccer)                                                                                          |
| 429         | Exact 2H Goals (Soccer)                                                                                             |
| 445         | Both Teams To Score In 1H/2H (Soccer)                                                                               |
| 446         | Home 1H To Score/2H To Score (Soccer)                                                                               |
| 447         | Away 1H To Score/2H To Score (Soccer)                                                                               |
| 18000       | Colossus Bets (Colossus Bets)                                                                                       |
| 18001       | Pool Bet (Colossus Bets)                                                                                            |
| 18002       | Cash Out (Colossus Bets)                                                                                            |
| 18004       | Consolation Prize (Colossus Bets)                                                                                   |
| 18005       | Jackpot Prize (Colossus Bets)                                                                                       |
| 601         | Winning Margin 14 Way (Basketball)                                                                                  |
| 602         | Winning Margin 12 Way (Basketball)                                                                                  |
| 603         | Which Team To Score The Highest Quarter (Basketball)                                                                |
| 604         | Which Team To Score The First Basket (Basketball)                                                                   |
| 605         | Which Team To Score The Last Basket (Basketball)                                                                    |
| 606         | 1H Race To X Points (Basketball)                                                                                    |
| 607         | 2H Race To X Points (Basketball)                                                                                    |
| 608         | 1H Winning Margin 13 Way (Basketball)                                                                               |
| 609         | Quarter X Handicap (Basketball)                                                                                     |
| 610         | Quarter X Over Under (Basketball)                                                                                   |
| 611         | Quarter X Odd Even (Basketball)                                                                                     |
| 612         | Quarter X Moneyline (Basketball)                                                                                    |
| 613         | Quarter X Race To Y Points (Basketball)                                                                             |
| 614         | Quarter X Winning Margin 7 Way (Basketball)                                                                         |
| 615         | Quarter X Home Team Over/Under (Basketball)                                                                         |
| 616         | Quarter X Away Team Over/Under (Basketball)                                                                         |
| 617         | Quarter X Which Team To Score The Last Basket (Basketball)                                                          |
| 618         | Last Digit Score (Basketball)                                                                                       |
| 619         | Home Team Last Digit Score (Basketball)                                                                             |
| 620         | Away Team Last Digit Score (Basketball)                                                                             |
| 621         | 1H Last Digit Score (Basketball)                                                                                    |
| 622         | 1H Home Team Last Digit Score (Basketball)                                                                          |
| 623         | 1H Away Team Last Digit Score (Basketball)                                                                          |
| 624         | 2H Last Digit Score (Basketball)                                                                                    |
| 625         | 2H Home Team Last Digit Score (Basketball)                                                                          |
| 626         | 2H Away Team Last Digit Score (Basketball)                                                                          |
| 627         | Quarter X Last Digit Score (Basketball)                                                                             |
| 628         | Quarter X Home Team Last Digit Score (Basketball)                                                                   |
| 629         | Quarter X Away Team Last Digit Score (Basketball)                                                                   |
| 630         | Quarter X 1X2 (Basketball)                                                                                          |
| 631         | Quarter X Double Chance (Basketball)                                                                                |
| 632         | Correct Quarter Odd/Even (Basketball)                                                                               |
| 633         | Quarter 1/Quarter 2 Result (Basketball)                                                                             |
| 634         | Quarter 3/Quarter 4 Result (Basketball)                                                                             |
| 635         | Quarter To Win To Nil (Basketball)                                                                                  |
| 636         | Quarter To Win From Behind (Basketball)                                                                             |
| 637         | Quarters Winners Handicap (Basketball)                                                                              |
| 638         | Home Team Quarters Win Over/Under (Basketball)                                                                      |
| 639         | Away Team Quarters Win Over/Under (Basketball)                                                                      |
| 640         | Exact Quarter Draw (Basketball)                                                                                     |
| 641         | Quarter Draw Over/Under (Basketball)                                                                                |
| 642         | Double Quarter Winner (Basketball)                                                                                  |
| 643         | Highest Scoring Quarter (Basketball)                                                                                |
| 644         | Quarter Correct Score (Basketball)                                                                                  |
| 645         | 1st Half/2nd Half Result (Basketball)                                                                               |
| 646         | Match Handicap & Total (Basketball)                                                                                 |
| 1011        | Fishing World (Fishing World)                                                                                       |
| 2401        | Baccarat (Allbet)                                                                                                   |
| 2402        | VIP Baccarat (Allbet)                                                                                               |
| 2404        | Sic Bo (Allbet)                                                                                                     |
| 2405        | Dragon Tiger (Allbet)                                                                                               |
| 2406        | Win 3 Cards (Allbet)                                                                                                |
| 2407        | Bull Bull (Allbet)                                                                                                  |
| 2408        | Pok Deng (Allbet)                                                                                                   |
| 1201        | Handicap (Virtual Soccer)                                                                                           |
| 1203        | Over/Under 2.5 Goals (Virtual Soccer)                                                                               |
| 1204        | Correct Score (Virtual Soccer)                                                                                      |
| 1205        | 1X2 (Virtual Soccer)                                                                                                |
| 1206        | Total Goal (Virtual Soccer)                                                                                         |
| 1220        | Moneyline (Virtual Tennis)                                                                                          |
| 1224        | Double chance (Virtual Soccer)                                                                                      |
| 1231        | Win (Virtual Sport)                                                                                                 |
| 1232        | Place (Virtual Sport)                                                                                               |
| 1233        | Win/Place (Virtual Sport)                                                                                           |
| 1235        | Correct Score (Virtual Tennis)                                                                                      |
| 1236        | Total Points (Virtual Tennis)                                                                                       |
| 1237        | Forecast (Virtual Motorsports)                                                                                      |
| 1238        | Tri-cast (Virtual Motorsports)                                                                                      |
| 2701        | FT. 1X2 (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)       |
| 2702        | 1H 1x2 (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)        |
| 2703        | Over/Under (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)    |
| 2704        | 1H Over/Under (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup) |
| 2705        | Handicap (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)      |
| 2706        | 1H Handicap (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)   |
| 2707        | Correct Score (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup) |
| 2799        | Mix Parlay (Virtual Soccer League, Virtual Soccer Nation, Virtual Soccer Champions Cup, Virtual Soccer Euro Cup)    |
| 2801        | Match Winner (Virtual Baskeball)                                                                                    |
| 2802        | 1H Winner (Virtual Baskeball)                                                                                       |
| 2803        | Over/Under (Virtual Baskeball)                                                                                      |
| 2804        | 1H Over/Under (Virtual Baskeball)                                                                                   |
| 2805        | Handicap (Virtual Baskeball)                                                                                        |
| 2806        | 1H Handicap (Virtual Baskeball)                                                                                     |
| 2807        | Winning Margin 6 Way (Virtual Baskeball)                                                                            |
| 2808        | 1H Winning Margin 7 Way (Virtual Baskeball)                                                                         |
| 2809        | FT Race To X Points (Virtual Baskeball)                                                                             |
| 2811        | Home Team Over/Under (Virtual Baskeball)                                                                            |
| 2812        | Away Team Over/Under (Virtual Baskeball)                                                                            |
| 461         | Home Team Over/Unde (Soccer)                                                                                        |
| 462         | Away Team Over/Under (Soccer)                                                                                       |
| 463         | 1H Home Team Over/Under (Soccer)                                                                                    |
| 464         | 1H Away Team Over/Under (Soccer)                                                                                    |
| 448         | Last Team To Score (Soccer)                                                                                         |
| 449         | Double Chance/Total Goals (Soccer)                                                                                  |
| 450         | Odd Even/Total Goals (Soccer)                                                                                       |
| 451         | Both Teams to Score / Double Chance (Soccer)                                                                        |
| 452         | Highest Scoring Half (2 Way) (Soccer)                                                                               |
| 453         | 1H 3-Way Handicap (Soccer)                                                                                          |
| 454         | Double Chance/First Team To Score (Soccer)                                                                          |
| 455         | Time of First Goal (Soccer)                                                                                         |
| 456         | 1H Both Teams To Score / Result (Soccer)                                                                            |
| 457         | 1H Both Teams To Score / Total Goals (Soccer)                                                                       |
| 458         | Asian 1X2 (Soccer)                                                                                                  |
| 459         | 1H Asian 1X2(Soccer)                                                                                                |
| 460         | Which Team Will Win By 5+ Goals (Soccer)                                                                            |
| 911         | Cash Out                                                                                                            |
| 8000        | Exchange                                                                                                            |
| 143         | 1H Result/Total Goals (Soccer)                                                                                      |
| 4101        | 888 Dragons™ (Pragmatic Play RNG Games)                                                                           |
| 4102        | Triple Tigers™ (Pragmatic Play RNG Games)                                                                         |
| 4103        | Diamonds are Forever 3 Lines (Pragmatic Play RNG Games)                                                             |
| 4104        | Gold Train™ (Pragmatic Play RNG Games)                                                                            |
| 4105        | 888 Gold™ (Pragmatic Play RNG Games)                                                                              |
| 4106        | 7 Piggies™ (Pragmatic Play RNG Games)                                                                             |
| 4107        | 7 Monkeys (Pragmatic Play RNG Games)                                                                                |
| 4108        | Monkey Madness™ (Pragmatic Play RNG Games)                                                                        |
| 4109        | Diamond Strike™ (Pragmatic Play RNG Games)                                                                        |
| 4110        | Fairytale Fortune™ (Pragmatic Play RNG Games)                                                                     |
| 4111        | 8 Dragons™ (Pragmatic Play RNG Games)                                                                             |
| 4112        | Lady Godiva (Pragmatic Play RNG Games)                                                                              |
| 4113        | Journey to the West (Pragmatic Play RNG Games)                                                                      |
| 4114        | Wolf Gold™ (Pragmatic Play RNG Games)                                                                             |
| 4115        | Dragon Kingdom (Pragmatic Play RNG Games)                                                                           |
| 4116        | Hot Safari (Pragmatic Play RNG Games)                                                                               |
| 4117        | 3 Kingdoms – Battle of Red Cliffs (Pragmatic Play RNG Games)                                                      |
| 4118        | Chilli Heat (Pragmatic Play RNG Games)                                                                              |
| 4119        | Caishen’s Gold™ (Pragmatic Play RNG Games)                                                                      |
| 4120        | Pixie Wings™ (Pragmatic Play RNG Games)                                                                           |
| 4121        | Aztec Gems (Pragmatic Play RNG Games)                                                                               |
| 4122        | Lucky New Year (Pragmatic Play RNG Games)                                                                           |
| 4123        | Busy Bees™ (Pragmatic Play RNG Games)                                                                             |
| 4124        | Fruity Blast™ (Pragmatic Play RNG Games)                                                                          |
| 4125        | Crazy 7s™ (Pragmatic Play RNG Games)                                                                              |
| 4126        | Devil’s 13™ (Pragmatic Play RNG Games)                                                                          |
| 4127        | Spooky Fortune™ (Pragmatic Play RNG Games)                                                                        |
| 4128        | Panda’s Fortune™ (Pragmatic Play RNG Games)                                                                     |
| 4129        | Money Roll™ (Pragmatic Play RNG Games)                                                                            |
| 4130        | Vegas Nights™ (Pragmatic Play RNG Games)                                                                          |
| 4131        | Wild Spells™ (Pragmatic Play RNG Games)                                                                           |
| 4132        | Jurassic Giants™ (Pragmatic Play RNG Games)                                                                       |
| 4133        | Panther Queen™ (Pragmatic Play RNG Games)                                                                         |
| 4134        | Irish Charms™ (Pragmatic Play RNG Games)                                                                          |
| 4135        | Queen of Atlantis™ (Pragmatic Play RNG Games)                                                                     |
| 4136        | Queen of Gold™ (Pragmatic Play RNG Games)                                                                         |
| 4137        | Hercules Son of Zeus (Pragmatic Play RNG Games)                                                                     |
| 4138        | 3 Genie Wishes (Pragmatic Play RNG Games)                                                                           |
| 4139        | The Catfather Part II (Pragmatic Play RNG Games)                                                                    |
| 4140        | Beowulf (Pragmatic Play RNG Games)                                                                                  |
| 4141        | Lucky Dragons (Pragmatic Play RNG Games)                                                                            |
| 4142        | Dwarven Gold Deluxe (Pragmatic Play RNG Games)                                                                      |
| 4143        | Romeo and Juliet (Pragmatic Play RNG Games)                                                                         |
| 4144        | Hockey League Wild Match (Pragmatic Play RNG Games)                                                                 |
| 4145        | Lady of the Moon (Pragmatic Play RNG Games)                                                                         |
| 4146        | KTV (Pragmatic Play RNG Games)                                                                                      |
| 4147        | Sugar Rush (Pragmatic Play RNG Games)                                                                               |
| 4148        | Sugar Rush Summer Time (Pragmatic Play RNG Games)                                                                   |
| 4149        | Sugar Rush Valentine’s Day (Pragmatic Play RNG Games)                                                             |
| 4150        | Sugar Rush Winter (Pragmatic Play RNG Games)                                                                        |
| 4151        | Tales of Egypt (Pragmatic Play RNG Games)                                                                           |
| 4152        | Hockey League (Pragmatic Play RNG Games)                                                                            |
| 4153        | Glorious Rome (Pragmatic Play RNG Games)                                                                            |
| 4154        | Magic Crystals (Pragmatic Play RNG Games)                                                                           |
| 4155        | Dwarven Gold (Pragmatic Play RNG Games)                                                                             |
| 4156        | Great Reef (Pragmatic Play RNG Games)                                                                               |
| 4157        | Aladdin’s Treasure (Pragmatic Play RNG Games)                                                                     |
| 4158        | Mighty Kong (Pragmatic Play RNG Games)                                                                              |
| 4159        | The Catfather (Pragmatic Play RNG Games)                                                                            |
| 4160        | Gold Rush™ (Pragmatic Play RNG Games)                                                                             |
| 4161        | Ancient Egypt (Pragmatic Play RNG Games)                                                                            |
| 4162        | Santa (Pragmatic Play RNG Games)                                                                                    |
| 4163        | Joker’s Jewels™ (Pragmatic Play RNG Games)                                                                      |
| 4164        | Fire 88™ (Pragmatic Play RNG Games)                                                                               |
| 4165        | The Champions™ (Pragmatic Play RNG Games)                                                                         |
| 4166        | 5 Lions™ (Pragmatic Play RNG Games)                                                                               |
| 4167        | Asgard™ (Pragmatic Play RNG Games)                                                                                |
| 4168        | Madame Destiny™ (Pragmatic Play RNG Games)                                                                        |
| 4169        | Great Rhino™ (Pragmatic Play RNG Games)                                                                           |
| 4170        | Jade Butterfly™ (Pragmatic Play RNG Games)                                                                        |
| 4171        | Leprechaun Song™ (Pragmatic Play RNG Games)                                                                       |
| 4172        | Da Vinci’s Treasure™ (Pragmatic Play RNG Games)                                                                 |
| 4173        | Master Chen’s Fortune™ (Pragmatic Play RNG Games)                                                               |
| 4174        | Vegas Magic™ (Pragmatic Play RNG Games)                                                                           |
| 4175        | Leprechaun Carol™ (Pragmatic Play RNG Games)                                                                      |
| 4176        | Mustang Gold™ (Pragmatic Play RNG Games)                                                                          |
| 4177        | Triple Dragons™ (Pragmatic Play RNG Games)                                                                        |
| 4178        | Safari King™ (Pragmatic Play RNG Games)                                                                           |
| 4179        | Treasure Horse™ (Pragmatic Play RNG Games)                                                                        |
| 4180        | Golden Pig™ (Pragmatic Play RNG Games)                                                                            |
| 4181        | Wild Gladiators™ (Pragmatic Play RNG Games)                                                                       |
| 4182        | Extra Juicy™ (Pragmatic Play RNG Games)                                                                           |
| 4183        | Egyptian Fortunes™ (Pragmatic Play RNG Games)                                                                     |
| 4184        | The Dog House™ (Pragmatic Play RNG Games)                                                                         |
| 4185        | Caishen’s Cash™ (Pragmatic Play RNG Games)                                                                      |
| 4186        | Sweet Bonanza™ (Pragmatic Play RNG Games)                                                                         |
| 4187        | 7 Monkeys JP (Pragmatic Play RNG Games)                                                                             |
| 4188        | Hot Safari JP (Pragmatic Play RNG Games)                                                                            |
| 4189        | The Catfather Part II JP (Pragmatic Play RNG Games)                                                                 |
| 4190        | Tree of Riches™ (Pragmatic Play RNG Games)                                                                        |
| 4191        | The Great Chicken Escape™ (Pragmatic Play RNG Games)                                                              |
| 4192        | Vampires vs Wolves™ (Pragmatic Play RNG Games)                                                                    |
| 4193        | Hot Chilli™ (Pragmatic Play RNG Games)                                                                            |
| 4194        | John Hunter and the Aztec Treasure™ (Pragmatic Play RNG Games)                                                    |
| 4195        | Monkey Warrior™(Pragmatic Play RNG Games)                                                                         |
| 4196        | Triple Jokers™ (Pragmatic Play RNG Games)                                                                         |
| 4197        | 5 Lions Gold™(Pragmatic Play RNG Games)                                                                           |
| 4198        | John Hunter and the Tomb of the Scarab Queen™ (Pragmatic Play RNG Games)                                          |
| 4199        | Super Joker™ (Pragmatic Play RNG Games)                                                                           |
| 4200        | Honey Honey Honey™ (Pragmatic Play RNG Games)                                                                     |
| 4201        | Fire Strike™ (Pragmatic Play RNG Games)                                                                           |
| 4202        | Hercules and Pegasus™ (Pragmatic Play RNG Games)                                                                  |
| 4203        | Sweet Bonanza Xmas™ (Pragmatic Play RNG Games)                                                                    |
| 4204        | Greek Gods™ (Pragmatic Play RNG Games)                                                                            |
| 4205        | Aladdin and the Sorcerer™ (Pragmatic Play RNG Games)                                                              |
| 4206        | Money Mouse™ (Pragmatic Play RNG Games)                                                                           |
| 4207        | Magic Journey™ (Pragmatic Play RNG Games)                                                                         |
| 4208        | Release the Kraken™ (Pragmatic Play RNG Games)                                                                    |
| 4209        | Super 7s™ (Pragmatic Play RNG Games)                                                                              |
| 4210        | Master Joker™ (Pragmatic Play RNG Games)                                                                          |
| 4211        | Golden Beauty™ (Pragmatic Play RNG Games)                                                                         |
| 4212        | Aztec Bonanza™ (Pragmatic Play RNG Games)                                                                         |
| 4213        | Mysterious™ (Pragmatic Play RNG Games)                                                                            |
| 4214        | The Wild Machine™ (Pragmatic Play RNG Games)                                                                      |
| 4215        | Wild West Gold™ (Pragmatic Play RNG Games)                                                                        |
| 4216        | Fruit Rainbow™ (Pragmatic Play RNG Games)                                                                         |
| 4217        | Dragon Ball™ (Pragmatic Play RNG Games)                                                                           |
| 4218        | Hot to Burn™ (Pragmatic Play RNG Games)                                                                           |
| 4219        | Three Star Fortune™ (Pragmatic Play RNG Games)                                                                    |
| 4220        | John Hunter and the Book of Tut™ (Pragmatic Play RNG Games)                                                       |
| 4221        | Fruit Party™ (Pragmatic Play RNG Games)                                                                           |
| 4222        | Starz Megaways™ (Pragmatic Play RNG Games)                                                                        |
| 4223        | Ultra Burn™ (Pragmatic Play RNG Games)                                                                            |
| 4224        | Bronco Spirit™ (Pragmatic Play RNG Games)                                                                         |
| 4225        | Money Money Money™ (Pragmatic Play RNG Games)                                                                     |
| 4226        | Pyramid King™ (Pragmatic Play RNG Games)                                                                          |
| 4227        | Fu Fu Fu™ (Pragmatic Play RNG Games)                                                                              |
| 4228        | Monkey Madness JP (Pragmatic Play RNG Games)                                                                        |
| 4229        | Aztec Gems JP (Pragmatic Play RNG Games)                                                                            |
| 4230        | 888 Dragons JP (Pragmatic Play RNG Games)                                                                           |
| 4231        | Journey to the West JP (Pragmatic Play RNG Games)                                                                   |
| 4232        | Street Racer™(Pragmatic Play RNG Games)                                                                           |
| 4233        | The Tiger Warrior™(Pragmatic Play RNG Games)                                                                      |
| 4234        | Great Rhino Deluxe (Pragmatic Play RNG Games)                                                                       |
| 4236        | The Dog House Megaways™(Pragmatic Play RNG Games)                                                                 |
| 4237        | Aztec Gems Deluxe™(Pragmatic Play RNG Games)                                                                      |
| 4238        | Jungle Gorilla™(Pragmatic Play RNG Games)                                                                         |
| 4239        | Curse of the Werewolf Megaways™(Pragmatic Play RNG Games)                                                         |
| 4240        | Ultra Hold and Spin™(Pragmatic Play RNG Games)                                                                    |
| 4242        | Drago - Jewels of Fortune™(Pragmatic Play RNG Games)                                                              |
| 4244        | Star Bounty™(Pragmatic Play RNG Games)                                                                            |
| 4245        | 5 Lions Dance™ (Pragmatic Play RNG Games)                                                                         |
| 4246        | Rise of Samurai™ (Pragmatic Play RNG Games)                                                                       |
| 4247        | Wild Walker™ (Pragmatic Play RNG Games)                                                                           |
| 4248        | Gems Bonanza™ (Pragmatic Play RNG Games)                                                                          |
| 4249        | Return of the Dead™ (Pragmatic Play RNG Games)                                                                    |
| 4250        | Book of Kingdoms™ (Pragmatic Play RNG Games)                                                                      |
| 4251        | Dragon Tiger™ (Pragmatic Play RNG Games)                                                                          |
| 4252        | Emerald King™ (Pragmatic Play RNG Games)                                                                          |
| 4253        | Pirate Gold Deluxe™ (Pragmatic Play RNG Games)                                                                    |
| 4254        | Cowboys Gold™ (Pragmatic Play RNG Games)                                                                          |
| 4256        | Spartan King™ (Pragmatic Play RNG Games)                                                                          |
| 4257        | John Hunter and the Mayan Gods™ (Pragmatic Play RNG Games)                                                        |
| 4258        | Big Bass Bonanza™ (Pragmatic Play RNG Games)                                                                      |
| 4259        | Golden Ox™ (Pragmatic Play RNG Games)                                                                             |
| 4260        | Emerald King Rainbow Road™ (Pragmatic Play RNG Games)                                                             |
| 4261        | Congo Cash™ (Pragmatic Play RNG Games)                                                                            |
| 4262        | Dragon Kingdom Eyes of Fire™ (Pragmatic Play RNG Games)                                                           |
| 4263        | Eye of the Storm™ (Pragmatic Play RNG Games)                                                                      |
| 4264        | Joker King™ (Pragmatic Play RNG Games)                                                                            |
| 4265        | Hot to Burn Hold and Spin™ (Pragmatic Play RNG Games)                                                             |
| 4297        | Jackpot Contribution (Pragmatic Play RNG Games)                                                                     |
| 4298        | Jackpot Prize (Pragmatic Play RNG Games)                                                                            |
| 4299        | Tournament Prize (Pragmatic Play RNG Games)                                                                         |
| 4300        | Promotion Prize (Pragmatic Play RNG Games)                                                                          |
| 4002        | BigHaul (Macau Games)                                                                                               |
| 4003        | BushidoCode (Macau Games)                                                                                           |
| 4004        | CasinoStudPoker (Macau Games)                                                                                       |
| 4005        | CatchtheWaves (Macau Games)                                                                                         |
| 4008        | DancingLions (Macau Games)                                                                                          |
| 4009        | DragonKing (Macau Games)                                                                                            |
| 4010        | DragonsandPearls (Macau Games)                                                                                      |
| 4011        | Fish’O’Mania (Macau Games)                                                                                      |
| 4012        | FortunePanda (Macau Games)                                                                                          |
| 4013        | GodofWealth (Macau Games)                                                                                           |
| 4014        | GoldofRa (Macau Games)                                                                                              |
| 4015        | GuardianLion (Macau Games)                                                                                          |
| 4018        | KingofTime (Macau Games)                                                                                            |
| 4019        | LadyLuck (Macau Games)                                                                                              |
| 4020        | MysticRiches (Macau Games)                                                                                          |
| 4021        | PhoenixPrincess (Macau Games)                                                                                       |
| 4022        | Roulette (Macau Games)                                                                                              |
| 4023        | TempleTreasures (Macau Games)                                                                                       |
| 4024        | ThaiDragon (Macau Games)                                                                                            |
| 4025        | TreasureReef (Macau Games)                                                                                          |
| 4026        | Venetia (Macau Games)                                                                                               |
| 4028        | WildDolphin (Macau Games)                                                                                           |
| 4029        | WolfQuest (Macau Games)                                                                                             |
| 4031        | Caishen’s Quest (Macau Games)                                                                                     |
| 4032        | Lucky Dragons (Macau Games)                                                                                         |
| 4033        | Highway Cash (Macau Games)                                                                                          |
| 4035        | Goldbeard’s Treasure (Macau Games)                                                                                |
| 4036        | Double Triple 8 (Macau Games)                                                                                       |
| 4037        | Gods of Fortune (Macau Games)                                                                                       |
| 4038        | Busy Bees (Macau Games)                                                                                             |
| 4039        | Funny Monkey (Macau Games)                                                                                          |
| 4040        | Gods of Fortune Deluxe (Macau Games)                                                                                |
| 4041        | Golf Tour (Macau Games)                                                                                             |
| 4042        | Lucky 8’s (Macau Games)                                                                                           |
| 4043        | Triple Lion (Macau Games)                                                                                           |
| 4044        | Dazzling Hot (Macau Games)                                                                                          |
| 4045        | Golden Koi (Macau Games)                                                                                            |
| 4046        | Triple Fortune (Macau Games)                                                                                        |
| 4047        | Twin Dragons (Macau Games)                                                                                          |
| 4048        | Luxury Life (Macau Games)                                                                                           |
| 4049        | Gold and Diamonds (Macau Games)                                                                                     |
| 4050        | Gold 88 (Macau Games)                                                                                               |
| 4051        | Vikings Unleashed Megaways (Macau Games)                                                                            |
| 4052        | Safari Gold Megaways (Macau Games)                                                                                  |
| 4053        | Buffalo Rising Megaways (Macau Games)                                                                               |
| 4054        | Return of Kong Megaways (Macau Games)                                                                               |
| 4055        | Wolf Legend Megaways (Macau Games)                                                                                  |
| 4056        | Genie Jackpots Megaways (Macau Games)                                                                               |
| 4057        | Valletta Megaways (Macau Games)                                                                                     |
| 4058        | Temple of Treasure Megaways (Macau Games)                                                                           |
| 4059        | Gods of Olympus Megaways (Macau Games)                                                                              |
| 4060        | Legacy of Ra Megaways (Macau Games)                                                                                 |
| 4061        | Diamond Mine Megaways (Macau Games)                                                                                 |
| 4062        | Fishin’ Frenzy Megaways (Macau Games)                                                                             |
| 4063        | Sweet Success Megaways (Macau Games)                                                                                |
| 4064        | Fortunes of Sparta (Macau Games)                                                                                    |
| 4065        | Imperial Dragon (Macau Games)                                                                                       |
| 4066        | King Kong Cash (Macau Games)                                                                                        |
| 4067        | Wish Upon a Leprechaun Megaways (Macau Games)                                                                       |
| 4068        | Super Spinner (Macau Games)                                                                                         |
| 4069        | Bar-X Safecracker Megaways (Macau Games)                                                                            |
| 4070        | Tiki Treasures Megaways (Macau Games)                                                                               |
| 4071        | Luck O the Irish Fortune Spins (Macau Games)                                                                        |
| 4072        | Caspers Mystery Mirror (Macau Games)                                                                                |
| 4073        | Dragonfall (Macau Games)                                                                                            |
| 4074        | Genie Jackpots (Macau Games)                                                                                        |
| 4075        | Primal Megaways (Macau Games)                                                                                       |
| 4076        | Luck O’ The Irish Megaways (Macau Games)                                                                          |
| 4077        | Diamond Mine: Extra Gold Megaways™(Macau Games)                                                                   |
| 4078        | Rick and Morty Megaways(Macau Games)                                                                                |
| 4079        | Gorilla Gold Megaways(Macau Games)                                                                                  |
| 4080        | Super Fortune Dragon(Macau Games)                                                                                   |
| 4081        | Golden Twins(Macau Games)                                                                                           |
| 4082        | Crystal Tiger(Macau Games)                                                                                          |
| 4083        | Zodiac Fortune(Macau Games)                                                                                         |
| 4084        | Golden Frog(Macau Games)                                                                                            |
| 4085        | Yellow Emperor(Macau Games)                                                                                         |
| 4086        | Crystal Candies(Macau Games)                                                                                        |
| 4087        | Dragon's Wealth(Macau Games)                                                                                        |
| 4088        | Legacy of the Gods Megaways(Macau Games)                                                                            |
| 4089        | Ted Megaways(Macau Games)                                                                                           |
| 4090        | Genie Jackpots Wishmaker(Macau Games)                                                                               |
| 9001        | Map X Moneyline (eSports)                                                                                           |
| 9002        | Map X Total Kills Handicap (eSports)                                                                                |
| 9003        | Map X Total Kills Over/Under (eSports)                                                                              |
| 9004        | Map X Total Kills Moneyline (eSports)                                                                               |
| 9005        | Map X Total Kills Odd/Even (eSports)                                                                                |
| 9006        | Map X First Blood (eSports)                                                                                         |
| 9007        | Map X First to Y Kills (eSports)                                                                                    |
| 9008        | Map X Total Towers Handicap (eSports)                                                                               |
| 9009        | Map X Total Towers Over/Under (eSports)                                                                             |
| 9010        | Map X Total Towers Moneyline (eSports)                                                                              |
| 9011        | Map X First Tier Y Tower (eSports)                                                                                  |
| 9012        | Map X Total Roshans Handicap (eSports)                                                                              |
| 9013        | Map X Total Roshans Over/Under (eSports)                                                                            |
| 9014        | Map X Total Roshans Moneyline (eSports)                                                                             |
| 9015        | Map X 1st Roshan (eSports)                                                                                          |
| 9016        | Map X 2nd Roshan (eSports)                                                                                          |
| 9017        | Map X 3rd Roshan (eSports)                                                                                          |
| 9018        | Map X Total Barracks Handicap (eSports)                                                                             |
| 9019        | Map X Total Barracks Over/Under (eSports)                                                                           |
| 9020        | Map X Total Barracks Moneyline (eSports)                                                                            |
| 9021        | Map X Barracks 1st Lane (eSports)                                                                                   |
| 9022        | Map X Barracks 2nd Lane (eSports)                                                                                   |
| 9023        | Map X Barracks 3rd Lane (eSports)                                                                                   |
| 9024        | Map X Total Turrets Handicap (eSports)                                                                              |
| 9025        | Map X Total Turrets Over/Under (eSports)                                                                            |
| 9026        | Map X Total Turrets Moneyline (eSports)                                                                             |
| 9027        | Map X First Tier Y Turret (eSports)                                                                                 |
| 9028        | Map X Total Dragons Handicap (eSports)                                                                              |
| 9029        | Map X Total Dragons Over/Under (eSports)                                                                            |
| 9030        | Map X Total Dragons Moneyline (eSports)                                                                             |
| 9031        | Map X 1st Dragon (eSports)                                                                                          |
| 9032        | Map X 2nd Dragon (eSports)                                                                                          |
| 9033        | Map X 3rd Dragon (eSports)                                                                                          |
| 9034        | Map X Total Barons Handicap (eSports)                                                                               |
| 9035        | Map X Total Barons Over/Under (eSports)                                                                             |
| 9036        | Map X Total Barons Moneyline (eSports)                                                                              |
| 9037        | Map X 1st Baron (eSports)                                                                                           |
| 9038        | Map X 2nd Baron (eSports)                                                                                           |
| 9039        | Map X 3rd Baron (eSports)                                                                                           |
| 9040        | Map X Total Inhibitors Handicap (eSports)                                                                           |
| 9041        | Map X Total Inhibitors Over/Under (eSports)                                                                         |
| 9042        | Map X Total Inhibitors Moneyline (eSports)                                                                          |
| 9043        | Map X 1st Inhibitor (eSports)                                                                                       |
| 9044        | Map X 2nd Inhibitor (eSports)                                                                                       |
| 9045        | Map X 3rd Inhibitor (eSports)                                                                                       |
| 9046        | Map X Total Tyrants Handicap (eSports)                                                                              |
| 9047        | Map X Total Tyrants Over/Under (eSports)                                                                            |
| 9048        | Map X Total Tyrants Moneyline (eSports)                                                                             |
| 9049        | Map X 1st Tyrant (eSports)                                                                                          |
| 9050        | Map X 2nd Tyrant (eSports)                                                                                          |
| 9051        | Map X 3rd Tyrant (eSports)                                                                                          |
| 9052        | Map X Total Overlords Handicap (eSports)                                                                            |
| 9053        | Map X Total Overlords Over/Under (eSports)                                                                          |
| 9054        | Map X Total Overlords Moneyline (eSports)                                                                           |
| 9055        | Map X 1st Overlord (eSports)                                                                                        |
| 9056        | Map X 2nd Overlord (eSports)                                                                                        |
| 9057        | Map X 3rd Overlord (eSports)                                                                                        |
| 9058        | Map X Duration Over/Under (Mins) (eSports)                                                                          |
| 9059        | Map X Rounds Handicap (eSports)                                                                                     |
| 9060        | Map X Rounds Over/Under (eSports)                                                                                   |
| 9061        | Map X Rounds Odd/Even (eSports)                                                                                     |
| 9062        | Map X First to Y Rounds (eSports)                                                                                   |
| 9063        | Map X First Half (eSports)                                                                                          |
| 9064        | Map X Second Half (eSports)                                                                                         |
| 9065        | Map X Most First Kill (eSports)                                                                                     |
| 9066        | Map X Clutches (eSports)                                                                                            |
| 9067        | Map X 16th Round (eSports)                                                                                          |
| 9068        | Map X Round Y Moneyline (eSports)                                                                                   |
| 9069        | Map X Round Y Total Kills Moneyline (eSports)                                                                       |
| 9070        | Map X Round Y Total Kills Over/Under (eSports)                                                                      |
| 9071        | Map X Round Y Total Kills Odd/Even (eSports)                                                                        |
| 9072        | Map X Round Y First Kill (eSports)                                                                                  |
| 9073        | Map X Round Y Bomb Plant (eSports)                                                                                  |
| 9075        | Map X Final Round Bomb Plant (eSports)                                                                              |
| 9076        | Map X Clutches Handicap (eSports)                                                                                   |
| 9077        | Map X Round Y Total Kills Handicap (eSports)                                                                        |
| 9078        | Map X Total Towers Odd/Even (eSports)                                                                               |
| 9079        | Map X Total Roshans Odd/Even (eSports)                                                                              |
| 9080        | Map X Total Barracks Odd/Even (eSports)                                                                             |
| 9081        | Map X Total Turrets Odd/Even (eSports)                                                                              |
| 9082        | Map X Total Dragons Odd/Even (eSports)                                                                              |
| 9083        | Map X Total Barons Odd/Even (eSports)                                                                               |
| 9084        | Map X Total Inhibitors Odd/Even (eSports)                                                                           |
| 9085        | Map X Total Tyrants Odd/Even (eSports)                                                                              |
| 9086        | Map X Total Overlords Odd/Even (eSports)                                                                            |
| 9087        | Map X Overtime (eSports)                                                                                            |
| 9088        | Correct Map Score (eSports)                                                                                         |
| 9400        | Super Over Winner (Cricket)                                                                                         |
| 9401        | Toss Winner (Cricket)                                                                                               |
| 9404        | Home Inns Runs (Cricket)                                                                                            |
| 9405        | Away Inns Runs (Cricket)                                                                                            |
| 9406        | Home 1st Inns Runs (Cricket)                                                                                        |
| 9407        | Away 1st Inns Runs (Cricket)                                                                                        |
| 9408        | Home 2nd Inns Runs (Cricket)                                                                                        |
| 9409        | Away 2nd Inns Runs (Cricket)                                                                                        |
| 9410        | Home Group 1-X Runs (Cricket)                                                                                       |
| 9411        | Away Group 1-X Runs (Cricket)                                                                                       |
| 9412        | Home 1st Inns Group 1-X Runs (Cricket)                                                                              |
| 9413        | Away 1st Inns Group 1-X Runs (Cricket)                                                                              |
| 9414        | Home 2nd Inns Group 1-X Runs (Cricket)                                                                              |
| 9415        | Away 2nd Inns Group 1-X Runs (Cricket)                                                                              |
| 9416        | Home Score at End of Over X (Cricket)                                                                               |
| 9417        | Away Score at End of Over X (Cricket)                                                                               |
| 9418        | Home 1st Inns Score at End of Over X (Cricket)                                                                      |
| 9419        | Away 1st Inns Score at End of Over X (Cricket)                                                                      |
| 9420        | Home 2nd Inns Score at End of Over X (Cricket)                                                                      |
| 9421        | Away 2nd Inns Score at End of Over X (Cricket)                                                                      |
| 9422        | Home Score after Over X Ball Y (Cricket)                                                                            |
| 9423        | Away Score after Over X Ball Y (Cricket)                                                                            |
| 9428        | Home Fall of Wicket X (Cricket)                                                                                     |
| 9429        | Away Fall of Wicket X (Cricket)                                                                                     |
| 9430        | Home 1st Inns Fall of Wicket X (Cricket)                                                                            |
| 9431        | Away 1st Inns Fall of Wicket X (Cricket)                                                                            |
| 9432        | Home 2nd Inns Fall of Wicket X (Cricket)                                                                            |
| 9433        | Away 2nd Inns Fall of Wicket X (Cricket)                                                                            |
| 9490        | Home Over X Ball Y Exact Runs (Cricket)                                                                             |
| 9491        | Away Over X Ball Y Exact Runs (Cricket)                                                                             |
| 9496        | Home Inns Runs OE (Cricket)                                                                                         |
| 9497        | Away Inns Runs OE (Cricket)                                                                                         |
| 9498        | Home 1st Inns Runs OE (Cricket)                                                                                     |
| 9499        | Away 1st Inns Runs OE (Cricket)                                                                                     |
| 9500        | Home 2nd Inns Runs OE (Cricket)                                                                                     |
| 9501        | Away 2nd Inns Runs OE (Cricket)                                                                                     |
| 9502        | Home Group 1-X Runs OE (Cricket)                                                                                    |
| 9503        | Away Group 1-X Runs OE (Cricket)                                                                                    |
| 9504        | Home 1st Inns Group 1-X Runs OE (Cricket)                                                                           |
| 9505        | Away 1st Inns Group 1-X Runs OE (Cricket)                                                                           |
| 9506        | Home 2nd Inns Group 1-X Runs OE (Cricket)                                                                           |
| 9507        | Away 2nd Inns Group 1-X Runs OE (Cricket)                                                                           |
| 9508        | Home Score at End of Over X OE (Cricket)                                                                            |
| 9509        | Away Score at End of Over X OE (Cricket)                                                                            |
| 9510        | Home 1st Inns Score at End of Over X OE (Cricket)                                                                   |
| 9511        | Away 1st Inns Score at End of Over X OE (Cricket)                                                                   |
| 9512        | Home 2nd Inns Score at End of Over X OE (Cricket)                                                                   |
| 9513        | Away 2nd Inns Score at End of Over X OE (Cricket)                                                                   |
| 9514        | Home Fall of Wicket X OE (Cricket)                                                                                  |
| 9515        | Away Fall of Wicket X OE (Cricket)                                                                                  |
| 9516        | Home 1st Inns Fall of Wicket X OE (Cricket)                                                                         |
| 9517        | Away 1st Inns Fall of Wicket X OE (Cricket)                                                                         |
| 9518        | Home 2nd Inns Fall of Wicket X OE (Cricket)                                                                         |
| 9519        | Away 2nd Inns Fall of Wicket X OE (Cricket)                                                                         |
| 9538        | 1X2 (Back & Lay) (Cricket)                                                                                          |
| 9539        | Match Winner (Back & Lay) (Cricket)                                                                                 |
| 9540        | Super Over Winner (Back & Lay) (Cricket)                                                                            |
| 8101        | Big/Small (Happy 5)                                                                                                 |
| 8102        | Odd/Even (Happy 5)                                                                                                  |
| 8103        | 4 Seasons (Happy 5)                                                                                                 |
| 8104        | More Odd/More Even (Happy 5)                                                                                        |
| 8105        | Combo (Happy 5)                                                                                                     |
| 3601        | Parlay 5 – Over/Under (Happy 5 Linked Games)                                                                      |
| 3602        | Parlay 5 – Odd/Even (Happy 5 Linked Games)                                                                        |
| 3603        | Parlay 5 – Red/Blue (Happy 5 Linked Games)                                                                        |
| 3604        | Soccer 5 (Happy 5 Linked Games)                                                                                     |
| 1901        | Video Poker (Arcadia Gaming)                                                                                        |
| 1902        | BigSmall (Arcadia Gaming)                                                                                           |
| 1903        | OddEven (Arcadia Gaming)                                                                                            |
| 1905        | 11HiLo (Arcadia Gaming)                                                                                             |
| 1906        | Bonus (Arcadia Gaming)                                                                                              |
| 1908        | RPS (Arcadia Gaming)                                                                                                |
| 1909        | Baccarat (Arcadia Gaming)                                                                                           |
| 1910        | Lucky Dash (Arcadia Gaming)                                                                                         |
| 1911        | GOAL!!! (Arcadia Gaming)                                                                                            |
| 1912        | Dragon Tiger (Arcadia Gaming)                                                                                       |
| 467         | Half Time/Full Time Exact Total Goals (Soccer)                                                                      |
| 468         | Score Box Handicap (Soccer)                                                                                         |
| 469         | Score Box Over/Under (Soccer)                                                                                       |
| 470         | Corners Odd/Even (Soccer)                                                                                           |
| 471         | 1H Corners Odd/Even (Soccer)                                                                                        |
| 472         | 2H Corners Odd/Even (Soccer)                                                                                        |
| 473         | Total Corners (Soccer)                                                                                              |
| 474         | 1H Total Corners (Soccer)                                                                                           |
| 475         | Alternative Corners (Soccer)                                                                                        |
| 476         | 1H Alternative Corners (Soccer)                                                                                     |
| 477         | Corner 3-Way Handicap (Soccer)                                                                                      |
| 478         | 1H Corner 3-Way Handicap (Soccer)                                                                                   |
| 479         | Time Of First Corner (Soccer)                                                                                       |
| 481         | Time Of 2H First Corner (Soccer)                                                                                    |
| 482         | Home Team Over/Under Corners (Soccer)                                                                               |
| 483         | Away Team Over/Under Corners (Soccer)                                                                               |
| 484         | 1H Home Team Over/Under Corners (Soccer)                                                                            |
| 485         | 1H Away Team Over/Under Corners (Soccer)                                                                            |
| 486         | Corners Race (Soccer)                                                                                               |
| 487         | 1H Corners Race (Soccer)                                                                                            |
| 488         | First Corner (Soccer)                                                                                               |
| 489         | 1H First Corner (Soccer)                                                                                            |
| 490         | 2H First Corner (Soccer)                                                                                            |
| 491         | Last Corner (Soccer)                                                                                                |
| 492         | 1H Last Corner (Soccer)                                                                                             |
| 493         | Half Time/Full Time Total Corners (Soccer)                                                                          |
| 494         | 1H Correct Corners (Soccer)                                                                                         |
| 495         | 2H Correct Corners (Soccer)                                                                                         |
| 496         | Corner Highest Scoring Half (Soccer)                                                                                |
| 497         | Corner Highest Scoring Half (2-Way )(Soccer)                                                                        |
| 701         | Match Handicap (Badminton)                                                                                          |
| 702         | Match Number of Games (Badminton)                                                                                   |
| 703         | Match Correct Score (Badminton)                                                                                     |
| 704         | Match Point Handicap (Badminton)                                                                                    |
| 705         | Match Point Over/Under (Badminton)                                                                                  |
| 706         | Match Point Odd/Even (Badminton)                                                                                    |
| 707         | Game X Winner (Badminton)                                                                                           |
| 708         | Game X Point Handicap (Badminton)                                                                                   |
| 709         | Game X Point Over/Under (Badminton)                                                                                 |
| 710         | Game X Point Odd/Even (Badminton)                                                                                   |
| 711         | Game X Winning Margin (Badminton)                                                                                   |
| 712         | Game X Race to Y Points (Badminton)                                                                                 |
| 713         | Game X Point Y Winner (Badminton)                                                                                   |
| 714         | Game X Extra Points (Badminton)                                                                                     |
| 376         | Penalty Shootout Combo (First 10) (Soccer)                                                                          |
| 381         | Away Penalty Shootout – Round X (Soccer)                                                                          |
| 382         | Home Penalty Shootout – Round X (Soccer)                                                                          |
| 383         | Penalty Shootout Woodwork – Round X (Soccer)                                                                      |
| 384         | Penalty Shootout Both/One/Neither – Round X (Soccer)                                                              |
| 385         | Penalty Shootout 1X2 – Round X (Soccer)                                                                           |
| 386         | Away Penalty Shootout Over/Under (First 10) (Soccer)                                                                |
| 387         | Home Penalty Shootout Over/Under (First 10) (Soccer)                                                                |
| 388         | Away Penalty Shootout Over/Under (Final Result) (Soccer)                                                            |
| 389         | Home Penalty Shootout Over/Under (Final Result) (Soccer)                                                            |
| 390         | Penalty Shootout to Go to Sudden Death (Soccer)                                                                     |
| 391         | Penalty Shootout Exact Total Goals (First 10) (Soccer)                                                              |
| 392         | Penalty Shootout Correct Score (Soccer)                                                                             |
| 393         | Penalty Shootout Odd/Even (First 10) (Soccer)                                                                       |
| 394         | Penalty Shootout Odd/Even (Final Result) (Soccer)                                                                   |
| 395         | Extra Time Next Goal Method (Soccer)                                                                                |
| 396         | Extra Time Next Goal (Soccer)                                                                                       |
| 397         | Extra Time Winning Margin (Soccer)                                                                                  |
| 398         | Extra Time Double Chance (Soccer)                                                                                   |
| 399         | Extra Time Correct Score (Soccer)                                                                                   |
| 400         | Extra Time Odd/Even (Soccer)                                                                                        |
| 38          | Single Match Parlay (Soccer/Basketball)                                                                             |
| 4301        | Tiger Warrior (SG)                                                                                                  |
| 4302        | Triple Panda (SG)                                                                                                   |
| 4303        | Gold Panther (SG)                                                                                                   |
| 4304        | Mr Chu Tycoon (SG)                                                                                                  |
| 4305        | Brothers kingdom (SG)                                                                                               |
| 4306        | Prosperity Gods (SG)                                                                                                |
| 4307        | Candy Pop (SG)                                                                                                      |
| 4308        | Golden Fist (SG)                                                                                                    |
| 4309        | FaFaFa2 (SG)                                                                                                        |
| 4310        | Gangster Axe (SG)                                                                                                   |
| 4311        | Princess Wang (SG)                                                                                                  |
| 4312        | Wow Prosperity (SG)                                                                                                 |
| 4314        | Golden Monkey (SG)                                                                                                  |
| 4315        | Jungle King (SG)                                                                                                    |
| 4316        | ZEUS (SG)                                                                                                           |
| 4317        | Ho Yeah Monkey (SG)                                                                                                 |
| 4318        | Baby Cai Shen (SG)                                                                                                  |
| 4319        | Cai Shen 888 (SG)                                                                                                   |
| 4320        | Double Fortunes (SG)                                                                                                |
| 4321        | 5 Fortune Dragons (SG)                                                                                              |
| 4322        | Lucky Koi (SG)                                                                                                      |
| 4323        | Fafafa (SG)                                                                                                         |
| 4324        | Da Fu Xiao Fu (SG)                                                                                                  |
| 4325        | Fist of Gold (SG)                                                                                                   |
| 4326        | Fishing God (SG)                                                                                                    |
| 4327        | 888 (SG)                                                                                                            |
| 4328        | Three Lucky Stars (SG)                                                                                              |
| 4329        | Pirate King (SG)                                                                                                    |
| 4330        | Highway Fortune (SG)                                                                                                |
| 4331        | Alibaba (SG)                                                                                                        |
| 4332        | Lucky Cai Shen (SG)                                                                                                 |
| 4333        | Drunken Jungle (SG)                                                                                                 |
| 4334        | Honey Hunter (SG)                                                                                                   |
| 4335        | Cai Yuan Guang Jin (SG)                                                                                             |
| 4336        | Daddy’s Vacation (SG)                                                                                             |
| 4337        | Magical Lamp (SG)                                                                                                   |
| 4338        | Money Mouse (SG)                                                                                                    |
| 4339        | Dancing Fever (SG)                                                                                                  |
| 4340        | King Pharaoh (SG)                                                                                                   |
| 4341        | Big Prosperity SA (SG)                                                                                              |
| 4342        | Dragon Gold SA (SG)                                                                                                 |
| 4343        | Mermaid (SG)                                                                                                        |
| 4344        | Sea Emperor (SG)                                                                                                    |
| 4345        | Festive Lion (SG)                                                                                                   |
| 4346        | Double Flame (SG)                                                                                                   |
| 4347        | Emperor Gate SA (SG)                                                                                                |
| 4348        | Iceland SA (SG)                                                                                                     |
| 4349        | 5 Fortune SA (SG)                                                                                                   |
| 4350        | Great Stars SA (SG)                                                                                                 |
| 4351        | Wong Choy SA (SG)                                                                                                   |
| 4352        | Pan Fairy (SG)                                                                                                      |
| 4353        | ShangHai 008 (SG)                                                                                                   |
| 4354        | Golden Chicken (SG)                                                                                                 |
| 4355        | God’s Kitchen (SG)                                                                                                |
| 4356        | Golf Champions (SG)                                                                                                 |
| 4357        | Golden Whale (SG)                                                                                                   |
| 4358        | Lucky Feng Shui (SG)                                                                                                |
| 4359        | Lucky Meow (SG)                                                                                                     |
| 4360        | Master Chef (SG)                                                                                                    |
| 4361        | Spins Stone (SG)                                                                                                    |
| 4362        | Santa Gifts (SG)                                                                                                    |
| 5001        | 4D (Togel 4D)                                                                                                       |
| 5002        | 3D (Togel 4D)                                                                                                       |
| 5003        | 2D Front (Togel 4D)                                                                                                 |
| 5004        | 2D Middle (Togel 4D)                                                                                                |
| 5005        | 2D Back (Togel 4D)                                                                                                  |
| 5006        | Colok Bebas (Togel 4D)                                                                                              |
| 5007        | Colok Jitu (Togel 4D)                                                                                               |
| 1612        | Big Small (Saba.club)                                                                                               |
| 1613        | Se Die (Saba.club)                                                                                                  |
| 1614        | Fish Prawn Crab (Saba.club)                                                                                         |
| 1615        | Turbo Big Small (Saba.club)                                                                                         |
| 1616        | Odd Even (Saba.club)                                                                                                |
| 1617        | Turbo Odd Even (Saba.club)                                                                                          |
| 1618        | Dragon Tiger (Saba.club)                                                                                            |
| 1619        | Keno Pro Max (Saba.club)                                                                                            |
| 1620        | Mini Se Die (Saba.club)                                                                                             |
| 1621        | Baccarat (Saba.club)                                                                                                |
| 1622        | Keno Pro Viet (Saba.club)                                                                                           |
| 1623        | Keno Max (Saba.club)                                                                                                |
| 1624        | Keno Max 2 (Saba.club)                                                                                              |
| 1625        | Keno Mini (Saba.club)                                                                                               |
| 1626        | Keno Mini 2 (Saba.club)                                                                                             |
| 1627        | Keno East (Saba.club)                                                                                               |
| 1628        | Keno West (Saba.club)                                                                                               |
| 1629        | Keno South (Saba.club)                                                                                              |
| 1630        | Keno North (Saba.club)                                                                                              |
| 1632        | Roulette (Saba.club)                                                                                                |
| 1633        | VIP Se Die (Saba.club)                                                                                              |
| 1634        | Blackjack (Saba.club)                                                                                               |
| 1635        | Special Se Die (Saba.club)                                                                                          |
| 1636        | Sic Bo (Saba.club)                                                                                                  |
| 1637        | VIP Fish Prawn Crab (Saba.club)                                                                                     |
| 1638        | VIP Baccarat (Saba.club)                                                                                            |
| 1639        | Se Die War (Saba.club)                                                                                              |
| 1640        | Four Guardians (Saba.club)                                                                                          |
| 1642        | RNG Lottery (Saba.club)                                                                                             |
| 1643        | Three Cards (Saba.club)                                                                                             |
| 1699        | Jackpot Prize (Saba.club)                                                                                           |
| 3101        | Baccarat (AE Sexy)                                                                                                  |
| 3102        | Dragon Tiger (AE Sexy)                                                                                              |
| 3103        | Sicbo (AE Sexy)                                                                                                     |
| 3104        | Roulette (AE Sexy)                                                                                                  |
| 3105        | Red Blue Duel (AE Sexy)                                                                                             |
| 6001        | Cai Shen Wins (PG Soft)                                                                                             |
| 6002        | Egypt Book Mystery (PG Soft)                                                                                        |
| 6003        | Ganesha Fortune (PG Soft)                                                                                           |
| 6004        | Dreams of Macau (PG Soft)                                                                                           |
| 6005        | Fortune Mouse (PG Soft)                                                                                             |
| 6006        | Leprechaun Riches (PG Soft)                                                                                         |
| 6007        | Candy Burst (PG Soft)                                                                                               |
| 6008        | Captain’s Bounty (PG Soft)                                                                                        |
| 6009        | Dragon Hatch (PG Soft)                                                                                              |
| 6010        | The Great Icescape (PG Soft)                                                                                        |
| 6011        | Phoenix Rises (PG Soft)                                                                                             |
| 6012        | Mahjong Ways (PG Soft)                                                                                              |
| 6013        | Piggy Gold (PG Soft)                                                                                                |
| 6014        | Ganesha Gold (PG Soft)                                                                                              |
| 6015        | Mahjong Ways 2 (PG Soft)                                                                                            |
| 6016        | Gem Saviour Conquest (PG Soft)                                                                                      |
| 6017        | Double Fortune (PG Soft)                                                                                            |
| 6018        | Muay Thai Champion (PG Soft)                                                                                        |
| 6019        | Dragon Tiger Luck (PG Soft)                                                                                         |
| 6020        | Jungle Delight (PG Soft)                                                                                            |
| 6021        | Win Win Won (PG Soft)                                                                                               |
| 6022        | Summon & Conquer (PG Soft)                                                                                          |
| 6023        | Dim Sum Mania (PG Soft)                                                                                             |
| 6024        | Honey Trap of Diao Chan (PG Soft)                                                                                   |
| 6025        | Gem Saviour (PG Soft)                                                                                               |
| 6026        | Fortune Gods (PG Soft)                                                                                              |
| 6027        | Medusa II (PG Soft)                                                                                                 |
| 6028        | Medusa (PG Soft)                                                                                                    |
| 6031        | Wizdom Wonders (PG Soft)                                                                                            |
| 6032        | Hood vs Wolf (PG Soft)                                                                                              |
| 6033        | Steampunk: Wheel of Destiny (PG Soft)                                                                               |
| 6034        | Reel Love (PG Soft)                                                                                                 |
| 6036        | Plushie Frenzy (PG Soft)                                                                                            |
| 6037        | Tree of Fortune (PG Soft)                                                                                           |
| 6038        | Hotpot (PG Soft)                                                                                                    |
| 6039        | Dragon Legend (PG Soft)                                                                                             |
| 6040        | Hip Hop Panda (PG Soft)                                                                                             |
| 6041        | Legend of Hou Yi (PG Soft)                                                                                          |
| 6042        | Mr.Hallow-Win (PG Soft)                                                                                             |
| 6043        | Prosperity Lion (PG Soft)                                                                                           |
| 6044        | Santa’s Gift Rush (PG Soft)                                                                                       |
| 6045        | Gem Saviour Sword (PG Soft)                                                                                         |
| 6046        | Symbols of Egypt (PG Soft)                                                                                          |
| 6047        | Three Monkeys (PG Soft)                                                                                             |
| 6048        | Emperor’s Favour (PG Soft)                                                                                        |
| 6050        | Journey to the Wealth (PG Soft)                                                                                     |
| 6051        | Ninja vs Samurai (PG Soft)                                                                                          |
| 6052        | Flirting Scholar (PG Soft)                                                                                          |
| 6053        | Shaolin Soccer (PG Soft)                                                                                            |
| 6055        | Bikini Paradise (PG Soft)                                                                                           |
| 6056        | Wild Fireworks (PG Soft)                                                                                            |
| 6057        | Genie’s 3 Wishes (PG Soft)                                                                                        |
| 6058        | Treasures of Aztec (PG Soft)                                                                                        |
| 6059        | Circus Delight (PG Soft)                                                                                            |
| 6060        | Thai River Wonders (PG Soft)                                                                                        |
| 6061        | Secret of Cleopatra (PG Soft)                                                                                       |
| 6062        | Vampires Charm (PG Soft)                                                                                            |
| 6063        | Queen of Bounty (PG Soft)                                                                                           |
| 6064        | Jewels of Prosperity (PG Soft)                                                                                      |
| 6065        | Lucky Neko (PG Soft)                                                                                                |
| 6066        | Jack Frost’s Winter (PG Soft)                                                                                     |
| 6067        | Galactic Gems (PG Soft)                                                                                             |
| 6068        | Guardians of Ice and Fire (PG Soft)                                                                                 |
| 6069        | Opera Dynasty (PG Soft)                                                                                             |
| 6070        | Fortune Ox (PG Soft)                                                                                                |
| 6071        | Bali Vacation (PG Soft)                                                                                             |
| 6072        | Crypto Gold (PG Soft)                                                                                               |
| 6073        | Majestic Treasures (PG Soft)                                                                                        |
| 6074        | Candy Bonanza (PG Soft)                                                                                             |
| 6075        | Wild Bandito (PG Soft)                                                                                              |
| 6076        | Ways of the Qilin (PG Soft)                                                                                         |
| 6077        | Heist Stakes (PG Soft)                                                                                              |
| 6401        | Book Of Ra (Joker)                                                                                                  |
| 6402        | Book Of Ra Deluxe (Joker)                                                                                           |
| 6403        | Columbus (Joker)                                                                                                    |
| 6404        | Dolphin Treasure (Joker)                                                                                            |
| 6405        | Dolphins Pearl Deluxe (Joker)                                                                                       |
| 6406        | Fifty Dragons (Joker)                                                                                               |
| 6407        | Fifty Lions (Joker)                                                                                                 |
| 6408        | Geisha (Joker)                                                                                                      |
| 6409        | Just Jewels (Joker)                                                                                                 |
| 6410        | Lord Of The Ocean (Joker)                                                                                           |
| 6411        | Lucky Lady Charm (Joker)                                                                                            |
| 6412        | Sizzling Hot (Joker)                                                                                                |
| 6413        | Cluster Mania (Joker)                                                                                               |
| 6414        | Power Stars (Joker)                                                                                                 |
| 6415        | Queen Of The Nile (Joker)                                                                                           |
| 3001        | Baccarat (SA Gaming)                                                                                                |
| 3002        | Dragon Tiger (SA Gaming)                                                                                            |
| 3003        | Sicbo (SA Gaming)                                                                                                   |
| 3004        | Fan Tan (SA Gaming)                                                                                                 |
| 3005        | Roulette (SA Gaming)                                                                                                |
| 3006        | Money Wheel (SA Gaming)                                                                                             |
| 3007        | Tips (SA Gaming)                                                                                                    |
| 3612        | Soccer Lottery (Sports Lottery)                                                                                     |
| 3802        | Big Small (Vgaming)                                                                                                 |
| 3803        | Se Die (Vgaming)                                                                                                    |
| 3804        | Fish Prawn Crab (Vgaming)                                                                                           |
| 3805        | Turbo Big Small (Vgaming)                                                                                           |
| 3806        | Odd Even (Vgaming)                                                                                                  |
| 3807        | Turbo Odd Even (Vgaming)                                                                                            |
| 3808        | Dragon Tiger (Vgaming)                                                                                              |
| 3809        | Keno Pro Max (Vgaming)                                                                                              |
| 3810        | Baccarat (Vgaming)                                                                                                  |
| 3811        | Keno Max (Vgaming)                                                                                                  |
| 3812        | Keno Max 2 (Vgaming)                                                                                                |
| 3813        | Keno Mini (Vgaming)                                                                                                 |
| 3814        | Keno Mini 2 (Vgaming)                                                                                               |
| 3815        | Keno East (Vgaming)                                                                                                 |
| 3816        | Keno West (Vgaming)                                                                                                 |
| 3817        | Keno South (Vgaming)                                                                                                |
| 3818        | Keno North (Vgaming)                                                                                                |
| 3819        | Roulette (Vgaming)                                                                                                  |
| 3820        | Blackjack (Vgaming)                                                                                                 |
| 3821        | Sic Bo (Vgaming)                                                                                                    |
| 3822        | Fish Prawn Crab PRO (Vgaming) (for VN market)                                                                       |
| 3823        | Mini Se Die (Vgaming) (for VN market)                                                                               |
| 3824        | VIP Se Die (Vgaming) (for VN market)                                                                                |
| 3825        | Special Se Die (Vgaming) (for VN market)                                                                            |
| 3826        | VIP Fish Prawn Crab (Vgaming) (for VN market)                                                                       |
| 3827        | VIP Baccarat (Vgaming) (for VN market)                                                                              |
| 3828        | Se Die War (Vgaming) (for VN market)                                                                                |
| 3829        | Four Guardians (Vgaming) (for VN market)                                                                            |
| 3830        | RNG Lottery (Vgaming) (for VN market)                                                                               |
| 3831        | Three Cards (Vgaming) (for VN market)                                                                               |
| 3832        | Baccarat Diamond (Vgaming)                                                                                          |
| 3833        | Baccarat Sapphire (Vgaming)                                                                                         |
| 3834        | Baccarat Ruby (Vgaming)                                                                                             |
| 3899        | Jackpot Prize (Vgaming)                                                                                             |
| 5701        | Baccarat (GDG)                                                                                                      |
| 5702        | SicBo (GDG)                                                                                                         |
| 5704        | Lucky 7 (GDG)                                                                                                       |
| 7301        | Richman (CG)                                                                                                        |
| 7302        | JurassicWorld (CG)                                                                                                  |
| 7303        | Halloween (CG)                                                                                                      |
| 7304        | Amazing Circus (CG)                                                                                                 |
| 7305        | Tai Wang Si Shen (CG)                                                                                               |
| 7306        | BreakAway (CG)                                                                                                      |
| 7307        | Platinum (CG)                                                                                                       |
| 7308        | Buffalo Bonus (CG)                                                                                                  |
| 7310        | Funky Monkey (CG)                                                                                                   |
| 7311        | Long Long Long (CG)                                                                                                 |
| 7312        | Fa Fa Fa (CG)                                                                                                       |
| 7313        | 777 (CG)                                                                                                            |
| 7314        | Chaoji 8 (CG)                                                                                                       |
| 7315        | Arcadia (CG)                                                                                                        |
| 7316        | City Of Poli (CG)                                                                                                   |
| 7317        | Dragon Skies (CG)                                                                                                   |
| 7318        | As The Gods Will (CG)                                                                                               |
| 7319        | Hungry Hungry Shark (CG)                                                                                            |
| 7320        | Jo ma ji (CG)                                                                                                       |
| 7321        | Tarzan (CG)                                                                                                         |
| 7323        | 50 Lions (CG)                                                                                                       |
| 7324        | Speed Racing (CG)                                                                                                   |
| 7325        | Marvel Tsum Tsum (CG)                                                                                               |
| 7326        | Line Brown Day dream (CG)                                                                                           |
| 7327        | Special Chef (CG)                                                                                                   |
| 7328        | Wu Lu Cai Shen (CG)                                                                                                 |
| 7329        | Fishing Expert (CG)                                                                                                 |
| 7330        | Zhao Cai Tong Zi (CG)                                                                                               |
(1170 rows)

