<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;


class BetAndSportTypeSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->file = [
            '/database/seeders/bet_sport.xlsx'
        ]; 
        
        // process all xlsx and csv files in paths specified above
        $this->extension = ['xlsx', 'csv'];
        
        parent::run();

    }
}
