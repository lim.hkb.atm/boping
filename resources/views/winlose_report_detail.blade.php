@extends('master')
@section('content')
<h2 class="text-center">BOPING TRANS DETAILS</h2>
<table id="table_detail" class="display">
    <thead>
        <tr>
            <th>No</th>
            <th>Transaction ID</th>
            <th>Winlost Datetime</th>
            <th>Sport Type</th>
            <th>Bet Type</th>
            <th>Player</th>
            <th>Odds</th>
            <th>Stake</th>
            <th>Ticket Status</th>
            <th>Winlost Amount</th>
            <th>Version Key</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list_boping_trans as $boping_trans)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $boping_trans->trans_id }}</td>
                <td>{{ $boping_trans->winlost_datetime }}</td>
                <td>{{ $boping_trans->sport->sport ?? null }}</td>
                <td>{{ $boping_trans->bet->bet_type_name }}</td>
                <td>{{ $boping_trans->player->vendor_member }}</td>
                <td>{{ $boping_trans->odds }}</td>
                <td>{{ $boping_trans->stake }}</td>
                <td>{{ $boping_trans->ticket_status }}</td>
                <td>{{ $boping_trans->winlost_amount }}</td>
                <td>{{ $boping_trans->version_key }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="7" style="text-align:center" id="total-text">TOTAL :</th>
            <th id="stake-total"></th>
            <th></th>
            <th id="win-lose-total"></th>
            <th></th>
        </tr>
    </tfoot>
</table>
@endsection
@section('script_1')
<script>
    $(document).ready(function(){
        var tableSellSummary = $('#table_detail').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                let total_text_col_index = parseInt(document.getElementById("total-text").getAttribute("colspan")) - 1;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                stake_column_index = total_text_col_index + parseInt(document.getElementById("stake-total").cellIndex);
                // Stake Total over all pages
                stake_total = api
                    .column(stake_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Stake Total over this page
                stake_page_total = api
                    .column(stake_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Stake footer
                $('#stake-total').html(
                    stake_page_total +' ('+ stake_total +' total)'
                );

                win_lose_column_index = total_text_col_index + parseInt(document.getElementById("win-lose-total").cellIndex);
                // Win / Lose Total over all pages
                win_lose_total = api
                    .column(win_lose_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Win / Lose Total over this page
                win_lose_page_total = api
                    .column(win_lose_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Win / lose footer
                $('#win-lose-total').html(
                    win_lose_page_total +' ('+ win_lose_total +' total)'
                );
            }
        });
    });
</script>
@endsection