@extends('master')
@section('content')
<div class="container-fluid">
<h2 class="text-center">SUMMARIZE WIN LOSE</h2>
<form action="/winlose-report" method="GET">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="start_date">Start Winlost Date</label>
            <input type="date" class="form-control" id="start_date" placeholder="Start Date" name="start_date" value="{{ request()->query('start_date') ? request()->query('start_date') : '' }}">
        </div>
        <div class="form-group col-md-3">
            <label for="end_date">End Winlost Date</label>
            <input type="date" class="form-control" id="end_date" placeholder="End Date" name="end_date" value="{{ request()->query('end_date') ? request()->query('end_date') : '' }}">
        </div>
        <div class="form-group col-md-3">
            <label for="sport_type">Sport Type</label>
            <select class="form-control" id="sport_type" name="sport_type">
                <option value="0">Select a Sport Type</option>
                @foreach ($sport_types as $sport_type)
                    <option value="{{ $sport_type->sport_id }}" {{ request()->query('sport_type') == $sport_type->sport_id ? 'selected' : '' }}>{{ $sport_type->sport }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="player">Player</label>
            <select class="form-control" id="player" name="player">
                <option value="0">Select a Player</option>
                @foreach ($players as $player)
                    <option value="{{ $player->id }}" {{ request()->query('player') == $player->id ? 'selected' : '' }}>{{ $player->vendor_member }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-row">
        <button type="submit" class="btn btn-primary mb-2">Filter</button>
    </div>
</form>
<table id="table_id" class="display">
    <thead>
        <tr>
            <th>No</th>
            <th>Winlost Datetime</th>
            <th>Sport Type</th>
            <th>Player</th>
            <th>Turnover amount</th>
            <th>Win</th>
            <th>Lose</th>
            <th>Win / Lose</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sum_winloses as $sum_winlose)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td><a href="/winlose-report-detail?winlost_datetime={{ $sum_winlose->winlost_datetime }}&sport_type={{ $sum_winlose->sport_type }}&vendor_member_id={{ $sum_winlose->vendor_member_id }}" target="_blank">{{ $sum_winlose->winlost_datetime }}</a></td>
                <td>{{ $sum_winlose->sport->sport ?? null }}</td>
                <td>{{ $sum_winlose->player->vendor_member }}</td>
                <td>{{ $sum_winlose->turnover_amount }}</td>
                <td>{{ $sum_winlose->win }}</td>
                <td>{{ $sum_winlose->lose }}</td>
                <td>{{ $sum_winlose->winlose }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="4" style="text-align:center" id="total-text">TOTAL :</th>
            <th id="stake-total"></th>
            <th id="win-total"></th>
            <th id="lose-total"></th>
            <th id="win-lose-total"></th>
        </tr>
    </tfoot>
</table>
</div>
@endsection
@section('script_1')
<script>
    $(document).ready(function(){
        var tableSellSummary = $('#table_id').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                let total_text_col_index = parseInt(document.getElementById("total-text").getAttribute("colspan")) - 1;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                stake_column_index = total_text_col_index + parseInt(document.getElementById("stake-total").cellIndex);
                // Stake Total over all pages
                stake_total = api
                    .column(stake_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Stake Total over this page
                stake_page_total = api
                    .column(stake_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Stake footer
                $('#stake-total').html(
                    stake_page_total +' ('+ stake_total +' total)'
                );

                win_column_index = total_text_col_index + parseInt(document.getElementById("win-total").cellIndex);
                // Win Total over all pages
                win_total = api
                    .column(win_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Win Total over this page
                win_page_total = api
                    .column(win_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update win footer
                $('#win-total').html(
                    win_page_total +' ('+ win_total +' total)'
                );

                lose_column_index = total_text_col_index + parseInt(document.getElementById("lose-total").cellIndex);
                // Lose Total over all pages
                lose_total = api
                    .column(lose_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Lose Total over this page
                lose_page_total = api
                    .column(lose_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update lose footer
                $('#lose-total').html(
                    lose_page_total +' ('+ lose_total +' total)'
                );

                win_lose_column_index = total_text_col_index + parseInt(document.getElementById("win-lose-total").cellIndex);
                // Win / Lose Total over all pages
                win_lose_total = api
                    .column(win_lose_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Win / Lose Total over this page
                win_lose_page_total = api
                    .column(win_lose_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Win / lose footer
                $('#win-lose-total').html(
                    win_lose_page_total +' ('+ win_lose_total +' total)'
                );
            }
        });
    });
</script>
@endsection