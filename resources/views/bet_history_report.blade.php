@extends('master')
@section('content')
<div class="container-fluid">
<h2 class="text-center">BET HISTORY</h2>
<form action="/bet-history-report" method="GET">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="transaction_start_datetime">Start Transaction Datetime</label>
            <input type="text" class="form-control" id="transaction_start_datetime" placeholder="Transaction Start Datetime" name="transaction_start_datetime">
        </div>
        <div class="form-group col-md-3">
            <label for="transaction_end_datetime">End Transaction Datetime</label>
            <input type="text" class="form-control" id="transaction_end_datetime" placeholder="Transaction End Datetime" name="transaction_end_datetime">
        </div>
        <div class="form-group col-md-3">
            <label for="sport_type">Sport Type</label>
            <select class="form-control" id="sport_type" name="sport_type">
                <option value="0">Select a Sport Type</option>
                @foreach ($sport_types as $sport_type)
                    <option value="{{ $sport_type->sport_id }}" {{ request()->query('sport_type') == $sport_type->sport_id ? 'selected' : '' }}>{{ $sport_type->sport }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="player">Player</label>
            <select class="form-control" id="player" name="player">
                <option value="0">Select a Player</option>
                @foreach ($players as $player)
                    <option value="{{ $player->id }}" {{ request()->query('player') == $player->id ? 'selected' : '' }}>{{ $player->vendor_member }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="match_start_datetime">Start Match Datetime</label>
            <input type="text" class="form-control" id="match_start_datetime" placeholder="Match Start Datetime" name="match_start_datetime">
        </div>
        <div class="form-group col-md-3">
            <label for="match_end_datetime">End Match Datetime</label>
            <input type="text" class="form-control" id="match_end_datetime" placeholder="Match End Datetime" name="match_end_datetime">
        </div>
        <div class="form-group col-md-3">
            <label for="settlement_start_datetime">Start Settlement Datetime</label>
            <input type="text" class="form-control" id="settlement_start_datetime" placeholder="Settlement Start Datetime" name="settlement_start_datetime">
        </div>
        <div class="form-group col-md-3">
            <label for="settlement_end_datetime">End Settlement Datetime</label>
            <input type="text" class="form-control" id="settlement_end_datetime" placeholder="Settlement End Datetime" name="settlement_end_datetime">
        </div>
    </div>
    <div class="form-row">
        <button type="submit" class="btn btn-primary mb-2">Filter</button>
    </div>
</form>
<table id="table_bet_history" class="display">
    <thead>
        <tr>
            <th>No</th>
            <th>Transaction Id</th>
            <th>Transaction Datetime</th>
            <th>Match Datetime</th>
            <th>Settlement Datetime</th>
            <th>Sport Type</th>
            <th>Bet Type</th>
            <th>Player</th>
            <th>Ticket Status</th>
            <th>League Name</th>
            <th>Turnover amount</th>
            <th>Win</th>
            <th>Lose</th>
            <th>Win / Lose</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list_boping_trans as $boping_trans)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $boping_trans->trans_id }}</td>
                <td>{{ $boping_trans->transaction_time }}</td>
                <td>{{ $boping_trans->match_datetime }}</td>
                <td>{{ $boping_trans->settlement_time }}</td>
                <td>{{ $boping_trans->sport->sport ?? null }}</td>
                <td>{{ $boping_trans->bet->bet_type_name }}</td>
                <td>{{ $boping_trans->player->vendor_member }}</td>
                <td>{{ $boping_trans->ticket_status }}</td>
                <td>{{ $boping_trans->leaguename ? json_decode($boping_trans->leaguename)[array_search("cs", array_column(json_decode($boping_trans->leaguename), "lang"))]->name : null }}</td>
                <td>{{ $boping_trans->stake }}</td>
                <td>{{ $boping_trans->winlost_amount > 0 ? $boping_trans->winlost_amount : 0 }}</td>
                <td>{{ $boping_trans->winlost_amount < 0 ? abs($boping_trans->winlost_amount) : 0 }}</td>
                <td>{{ $boping_trans->winlost_amount }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th colspan="10" style="text-align:center" id="total-text">TOTAL :</th>
            <th id="stake-total"></th>
            <th id="win-total"></th>
            <th id="lose-total"></th>
            <th id="win-lose-total"></th>
        </tr>
    </tfoot>
</table>
</div>
@endsection
@section('script_1')
<script>
    $(document).ready(function(){
        // List of input field id
        let list_datetime_id = `
            #transaction_start_datetime, #transaction_end_datetime, 
            #match_start_datetime, #match_end_datetime, 
            #settlement_start_datetime, #settlement_end_datetime
        `;

        // Initialize daterangepicker on input field
        $(list_datetime_id).daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss',
                cancelLabel: 'Clear'
            },
            timePicker24Hour: true,
            timePickerSeconds: true,
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm:ss') + ' to ' + end.format('YYYY-MM-DD'));
        });

        // Source: https://flaviocopes.com/urlsearchparams/
        // Get query string
        const params = new URLSearchParams(window.location.search);
        $('#transaction_start_datetime').val(params.get('transaction_start_datetime'));
        $('#transaction_end_datetime').val(params.get('transaction_end_datetime'));
        $('#match_start_datetime').val(params.get('match_start_datetime'));
        $('#match_end_datetime').val(params.get('match_end_datetime'));
        $('#settlement_start_datetime').val(params.get('settlement_start_datetime'));
        $('#settlement_end_datetime').val(params.get('settlement_end_datetime'));
        
        // Clear daterangepicker input field
        $(list_datetime_id).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        var tableSellSummary = $('#table_bet_history').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                let total_text_col_index = parseInt(document.getElementById("total-text").getAttribute("colspan")) - 1;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                stake_column_index = total_text_col_index + parseInt(document.getElementById("stake-total").cellIndex);
                // Stake Total over all pages
                stake_total = api
                    .column(stake_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Stake Total over this page
                stake_page_total = api
                    .column(stake_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Stake footer
                $('#stake-total').html(
                    stake_page_total +' ('+ stake_total +' total)'
                );

                win_column_index = total_text_col_index + parseInt(document.getElementById("win-total").cellIndex);
                // Win Total over all pages
                win_total = api
                    .column(win_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Win Total over this page
                win_page_total = api
                    .column(win_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update win footer
                $('#win-total').html(
                    win_page_total +' ('+ win_total +' total)'
                );

                lose_column_index = total_text_col_index + parseInt(document.getElementById("lose-total").cellIndex);
                // Lose Total over all pages
                lose_total = api
                    .column(lose_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Lose Total over this page
                lose_page_total = api
                    .column(lose_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update lose footer
                $('#lose-total').html(
                    lose_page_total +' ('+ lose_total +' total)'
                );

                win_lose_column_index = total_text_col_index + parseInt(document.getElementById("win-lose-total").cellIndex);
                // Win / Lose Total over all pages
                win_lose_total = api
                    .column(win_lose_column_index)
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Win / Lose Total over this page
                win_lose_page_total = api
                    .column(win_lose_column_index, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return new Big(a).plus(b);
                    }, 0 ).toString();
    
                // Update Win / lose footer
                $('#win-lose-total').html(
                    win_lose_page_total +' ('+ win_lose_total +' total)'
                );
            }
        });
        
    });
</script>
@endsection